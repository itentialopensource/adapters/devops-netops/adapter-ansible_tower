/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-ansible_tower',
      type: 'AnsibleTower',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const AnsibleTower = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Ansible_tower Adapter Test', () => {
  describe('AnsibleTower Class Tests', () => {
    const a = new AnsibleTower(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#versioningList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.versioningList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.available_versions);
                assert.equal('/api/v2/', data.response.current_version);
                assert.equal('', data.response.custom_login_info);
                assert.equal('', data.response.custom_logo);
                assert.equal('AWX REST API', data.response.description);
                assert.equal('/api/o/', data.response.oauth2);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Versioning', 'versioningList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#versioningRead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.versioningRead((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('/api/v2/activity_stream/', data.response.activity_stream);
                assert.equal('/api/v2/ad_hoc_commands/', data.response.ad_hoc_commands);
                assert.equal('/api/v2/applications/', data.response.applications);
                assert.equal('/api/v2/config/', data.response.config);
                assert.equal('/api/v2/credential_input_sources/', data.response.credential_input_sources);
                assert.equal('/api/v2/credential_types/', data.response.credential_types);
                assert.equal('/api/v2/credentials/', data.response.credentials);
                assert.equal('/api/v2/dashboard/', data.response.dashboard);
                assert.equal('/api/v2/groups/', data.response.groups);
                assert.equal('/api/v2/hosts/', data.response.hosts);
                assert.equal('/api/v2/instance_groups/', data.response.instance_groups);
                assert.equal('/api/v2/instances/', data.response.instances);
                assert.equal('/api/v2/inventories/', data.response.inventory);
                assert.equal('/api/v2/inventory_scripts/', data.response.inventory_scripts);
                assert.equal('/api/v2/inventory_sources/', data.response.inventory_sources);
                assert.equal('/api/v2/inventory_updates/', data.response.inventory_updates);
                assert.equal('/api/v2/job_events/', data.response.job_events);
                assert.equal('/api/v2/job_templates/', data.response.job_templates);
                assert.equal('/api/v2/jobs/', data.response.jobs);
                assert.equal('/api/v2/labels/', data.response.labels);
                assert.equal('/api/v2/me/', data.response.me);
                assert.equal('/api/v2/metrics/', data.response.metrics);
                assert.equal('/api/v2/notification_templates/', data.response.notification_templates);
                assert.equal('/api/v2/notifications/', data.response.notifications);
                assert.equal('/api/v2/organizations/', data.response.organizations);
                assert.equal('/api/v2/ping/', data.response.ping);
                assert.equal('/api/v2/project_updates/', data.response.project_updates);
                assert.equal('/api/v2/projects/', data.response.projects);
                assert.equal('/api/v2/roles/', data.response.roles);
                assert.equal('/api/v2/schedules/', data.response.schedules);
                assert.equal('/api/v2/settings/', data.response.settings);
                assert.equal('/api/v2/system_job_templates/', data.response.system_job_templates);
                assert.equal('/api/v2/system_jobs/', data.response.system_jobs);
                assert.equal('/api/v2/teams/', data.response.teams);
                assert.equal('/api/v2/tokens/', data.response.tokens);
                assert.equal('/api/v2/unified_job_templates/', data.response.unified_job_templates);
                assert.equal('/api/v2/unified_jobs/', data.response.unified_jobs);
                assert.equal('/api/v2/users/', data.response.users);
                assert.equal('/api/v2/workflow_job_nodes/', data.response.workflow_job_nodes);
                assert.equal('/api/v2/workflow_job_template_nodes/', data.response.workflow_job_template_nodes);
                assert.equal('/api/v2/workflow_job_templates/', data.response.workflow_job_templates);
                assert.equal('/api/v2/workflow_jobs/', data.response.workflow_jobs);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Versioning', 'versioningRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let authenticationId = 'fakedata';
    const authenticationAuthenticationApplicationsCreate0BodyParam = {};
    describe('#authenticationApplicationsCreate0 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.authenticationApplicationsCreate0(authenticationAuthenticationApplicationsCreate0BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('password', data.response.authorization_grant_type);
                assert.equal('xxxx', data.response.client_id);
                assert.equal('KEuoiuRzGZpWkVG9kbfPNpPRDSo0Nlo9IpJaSxdLsPAAjUIyhBeM8PdRdGVcbM04xvRWhmTTyALBbK0widBNZ3sK3HaqeEPwwn6A89H1Mq8OMTxehZP5piHWEz6dOyiH', data.response.client_secret);
                assert.equal('confidential', data.response.client_type);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('test app', data.response.name);
                assert.equal(1, data.response.organization);
                assert.equal('', data.response.redirect_uris);
                assert.equal('object', typeof data.response.related);
                assert.equal(false, data.response.skip_authorization);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('o_auth2_application', data.response.type);
                assert.equal('/api/v2/applications/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              authenticationId = data.response.id;
              saveMockData('Authentication', 'authenticationApplicationsCreate0', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationAuthenticationApplicationsTokensCreate0BodyParam = {};
    describe('#authenticationApplicationsTokensCreate0 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.authenticationApplicationsTokensCreate0(authenticationId, authenticationAuthenticationApplicationsTokensCreate0BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.application);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.expires);
                assert.equal(1, data.response.id);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('1cp85sj6OXAX5jjroUIeBsiwg3sLju', data.response.refresh_token);
                assert.equal('object', typeof data.response.related);
                assert.equal('read', data.response.scope);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('y3UrG2YvzgZ1ZNr5AbC3VwwyolbgvU', data.response.token);
                assert.equal('o_auth2_access_token', data.response.type);
                assert.equal('/api/v2/tokens/1/', data.response.url);
                assert.equal(1, data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticationApplicationsTokensCreate0', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationAuthenticationTokensCreate0BodyParam = {};
    describe('#authenticationTokensCreate0 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.authenticationTokensCreate0(authenticationAuthenticationTokensCreate0BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.application);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.expires);
                assert.equal(2, data.response.id);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('AlnjDfWNO5NXpKXA8SrAnx58XwoFvh', data.response.refresh_token);
                assert.equal('object', typeof data.response.related);
                assert.equal('read', data.response.scope);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('LJ4bOnQPPvy2icUgwKTRY0kJFgAZ4E', data.response.token);
                assert.equal('o_auth2_access_token', data.response.type);
                assert.equal('/api/v2/tokens/2/', data.response.url);
                assert.equal(1, data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticationTokensCreate0', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationOList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authenticationOList((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticationOList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationApplicationsList0 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authenticationApplicationsList0(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticationApplicationsList0', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationAuthenticationApplicationsUpdate0BodyParam = {
      authorization_grant_type: 'string',
      client_type: 'string',
      name: 'string',
      organization: 6
    };
    describe('#authenticationApplicationsUpdate0 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authenticationApplicationsUpdate0(authenticationId, null, authenticationAuthenticationApplicationsUpdate0BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticationApplicationsUpdate0', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationAuthenticationApplicationsPartialUpdate0BodyParam = {};
    describe('#authenticationApplicationsPartialUpdate0 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.authenticationApplicationsPartialUpdate0(authenticationId, null, authenticationAuthenticationApplicationsPartialUpdate0BodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticationApplicationsPartialUpdate0', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationApplicationsRead0 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.authenticationApplicationsRead0(authenticationId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('password', data.response.authorization_grant_type);
                assert.equal('xxxx', data.response.client_id);
                assert.equal('************', data.response.client_secret);
                assert.equal('confidential', data.response.client_type);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('test app', data.response.name);
                assert.equal('', data.response.redirect_uris);
                assert.equal('object', typeof data.response.related);
                assert.equal(false, data.response.skip_authorization);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('o_auth2_application', data.response.type);
                assert.equal('/api/v2/applications/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticationApplicationsRead0', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationApplicationsActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authenticationApplicationsActivityStreamList(authenticationId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticationApplicationsActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationApplicationsTokensList0 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.authenticationApplicationsTokensList0(authenticationId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(0, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticationApplicationsTokensList0', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationTokensList0 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authenticationTokensList0(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticationTokensList0', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationAuthenticationTokensUpdateBodyParam = {
      description: 'string',
      scope: 'string'
    };
    describe('#authenticationTokensUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authenticationTokensUpdate(authenticationId, null, authenticationAuthenticationTokensUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticationTokensUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationAuthenticationTokensPartialUpdateBodyParam = {};
    describe('#authenticationTokensPartialUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.authenticationTokensPartialUpdate(authenticationId, null, authenticationAuthenticationTokensPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticationTokensPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationTokensRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authenticationTokensRead(authenticationId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticationTokensRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationTokensActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authenticationTokensActivityStreamList(authenticationId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticationTokensActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activityStreamsActivityStreamList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.activityStreamsActivityStreamList(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActivityStreams', 'activityStreamsActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#activityStreamsActivityStreamRead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.activityStreamsActivityStreamRead('fakedata', null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('awx', data.response.action_node);
                assert.equal('object', typeof data.response.changes);
                assert.equal(7, data.response.id);
                assert.equal('user', data.response.object1);
                assert.equal('inventory', data.response.object2);
                assert.equal('role', data.response.object_association);
                assert.equal('inventory', data.response.object_type);
                assert.equal('associate', data.response.operation);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.timestamp);
                assert.equal('activity_stream', data.response.type);
                assert.equal('/api/v2/activity_stream/7/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ActivityStreams', 'activityStreamsActivityStreamRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandEventsAdHocCommandEventsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.adHocCommandEventsAdHocCommandEventsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AdHocCommandEvents', 'adHocCommandEventsAdHocCommandEventsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const adHocCommandEventsId = 'fakedata';
    describe('#adHocCommandEventsAdHocCommandEventsRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.adHocCommandEventsAdHocCommandEventsRead(adHocCommandEventsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AdHocCommandEvents', 'adHocCommandEventsAdHocCommandEventsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let adHocCommandsId = 'fakedata';
    const adHocCommandsAdHocCommandsAdHocCommandsCreateBodyParam = {};
    describe('#adHocCommandsAdHocCommandsCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.adHocCommandsAdHocCommandsCreate(adHocCommandsAdHocCommandsAdHocCommandsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.become_enabled);
                assert.equal('', data.response.controller_node);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal(1, data.response.credential);
                assert.equal(false, data.response.diff_mode);
                assert.equal(0, data.response.elapsed);
                assert.equal('', data.response.execution_node);
                assert.equal('', data.response.extra_vars);
                assert.equal(false, data.response.failed);
                assert.equal(0, data.response.forks);
                assert.equal(2, data.response.id);
                assert.equal(3, data.response.inventory);
                assert.equal('', data.response.job_explanation);
                assert.equal('run', data.response.job_type);
                assert.equal('manual', data.response.launch_type);
                assert.equal('', data.response.limit);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('uptime', data.response.module_args);
                assert.equal('command', data.response.module_name);
                assert.equal('command', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('new', data.response.status);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('ad_hoc_command', data.response.type);
                assert.equal('/api/v2/ad_hoc_commands/2/', data.response.url);
                assert.equal(0, data.response.verbosity);
              } else {
                runCommonAsserts(data, error);
              }
              adHocCommandsId = data.response.id;
              saveMockData('AdHocCommands', 'adHocCommandsAdHocCommandsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsCancelCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.adHocCommandsAdHocCommandsCancelCreate(adHocCommandsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AdHocCommands', 'adHocCommandsAdHocCommandsCancelCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsRelaunchCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.adHocCommandsAdHocCommandsRelaunchCreate(adHocCommandsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AdHocCommands', 'adHocCommandsAdHocCommandsRelaunchCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.adHocCommandsAdHocCommandsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AdHocCommands', 'adHocCommandsAdHocCommandsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsRead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.adHocCommandsAdHocCommandsRead(adHocCommandsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.become_enabled);
                assert.equal('', data.response.controller_node);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal(false, data.response.diff_mode);
                assert.equal(0, data.response.elapsed);
                assert.equal(false, data.response.event_processing_finished);
                assert.equal('', data.response.execution_node);
                assert.equal('', data.response.extra_vars);
                assert.equal(false, data.response.failed);
                assert.equal(0, data.response.forks);
                assert.equal('object', typeof data.response.host_status_counts);
                assert.equal(4, data.response.id);
                assert.equal(1, data.response.inventory);
                assert.equal('', data.response.job_args);
                assert.equal('', data.response.job_cwd);
                assert.equal('object', typeof data.response.job_env);
                assert.equal('', data.response.job_explanation);
                assert.equal('run', data.response.job_type);
                assert.equal('manual', data.response.launch_type);
                assert.equal('', data.response.limit);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('', data.response.module_args);
                assert.equal('', data.response.module_name);
                assert.equal('', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('', data.response.result_traceback);
                assert.equal('new', data.response.status);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('ad_hoc_command', data.response.type);
                assert.equal('/api/v2/ad_hoc_commands/4/', data.response.url);
                assert.equal(0, data.response.verbosity);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AdHocCommands', 'adHocCommandsAdHocCommandsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.adHocCommandsAdHocCommandsActivityStreamList(adHocCommandsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AdHocCommands', 'adHocCommandsAdHocCommandsActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsCancelRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.adHocCommandsAdHocCommandsCancelRead(adHocCommandsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AdHocCommands', 'adHocCommandsAdHocCommandsCancelRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsEventsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.adHocCommandsAdHocCommandsEventsList(adHocCommandsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AdHocCommands', 'adHocCommandsAdHocCommandsEventsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsNotificationsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.adHocCommandsAdHocCommandsNotificationsList(adHocCommandsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AdHocCommands', 'adHocCommandsAdHocCommandsNotificationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsRelaunchList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.adHocCommandsAdHocCommandsRelaunchList(adHocCommandsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AdHocCommands', 'adHocCommandsAdHocCommandsRelaunchList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsStdoutRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.adHocCommandsAdHocCommandsStdoutRead(adHocCommandsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AdHocCommands', 'adHocCommandsAdHocCommandsStdoutRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemConfigurationConfigCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemConfigurationConfigCreate((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemConfiguration', 'systemConfigurationConfigCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemConfigurationAuthList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemConfigurationAuthList((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemConfiguration', 'systemConfigurationAuthList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemConfigurationConfigList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.systemConfigurationConfigList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('off', data.response.analytics_status);
                assert.equal('2.9.0.dev0', data.response.ansible_version);
                assert.equal(true, Array.isArray(data.response.become_methods));
                assert.equal(true, Array.isArray(data.response.custom_virtualenvs));
                assert.equal('', data.response.eula);
                assert.equal('object', typeof data.response.license_info);
                assert.equal('/projects/', data.response.project_base_dir);
                assert.equal(true, Array.isArray(data.response.project_local_paths));
                assert.equal('UTC', data.response.time_zone);
                assert.equal('6.1.0-215-gad17bdc55', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemConfiguration', 'systemConfigurationConfigList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemConfigurationPingList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.systemConfigurationPingList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('awx', data.response.active_node);
                assert.equal(false, data.response.ha);
                assert.equal('00000000-0000-0000-0000-000000000000', data.response.install_uuid);
                assert.equal(true, Array.isArray(data.response.instance_groups));
                assert.equal(true, Array.isArray(data.response.instances));
                assert.equal('6.1.0-215-gad17bdc55', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemConfiguration', 'systemConfigurationPingList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let credentialInputSourcesId = 'fakedata';
    const credentialInputSourcesCredentialInputSourcesCredentialInputSourcesCreateBodyParam = {};
    describe('#credentialInputSourcesCredentialInputSourcesCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.credentialInputSourcesCredentialInputSourcesCreate(credentialInputSourcesCredentialInputSourcesCredentialInputSourcesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal('password', data.response.input_field_name);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('object', typeof data.response.related);
                assert.equal(2, data.response.source_credential);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(1, data.response.target_credential);
                assert.equal('credential_input_source', data.response.type);
                assert.equal('/api/v2/credential_input_sources/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              credentialInputSourcesId = data.response.id;
              saveMockData('CredentialInputSources', 'credentialInputSourcesCredentialInputSourcesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialInputSourcesCredentialInputSourcesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.credentialInputSourcesCredentialInputSourcesList(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(0, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CredentialInputSources', 'credentialInputSourcesCredentialInputSourcesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialInputSourcesCredentialInputSourcesCredentialInputSourcesUpdateBodyParam = {
      input_field_name: 'string',
      source_credential: 10,
      target_credential: 8
    };
    describe('#credentialInputSourcesCredentialInputSourcesUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialInputSourcesCredentialInputSourcesUpdate(credentialInputSourcesId, null, credentialInputSourcesCredentialInputSourcesCredentialInputSourcesUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CredentialInputSources', 'credentialInputSourcesCredentialInputSourcesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialInputSourcesCredentialInputSourcesCredentialInputSourcesPartialUpdateBodyParam = {};
    describe('#credentialInputSourcesCredentialInputSourcesPartialUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.credentialInputSourcesCredentialInputSourcesPartialUpdate(credentialInputSourcesId, null, credentialInputSourcesCredentialInputSourcesCredentialInputSourcesPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CredentialInputSources', 'credentialInputSourcesCredentialInputSourcesPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialInputSourcesCredentialInputSourcesRead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.credentialInputSourcesCredentialInputSourcesRead(credentialInputSourcesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal('vault_password', data.response.input_field_name);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('object', typeof data.response.related);
                assert.equal(2, data.response.source_credential);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(1, data.response.target_credential);
                assert.equal('credential_input_source', data.response.type);
                assert.equal('/api/v2/credential_input_sources/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CredentialInputSources', 'credentialInputSourcesCredentialInputSourcesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let credentialTypesId = 'fakedata';
    const credentialTypesCredentialTypesCredentialTypesCreateBodyParam = {};
    describe('#credentialTypesCredentialTypesCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.credentialTypesCredentialTypesCreate(credentialTypesCredentialTypesCredentialTypesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal('object', typeof data.response.injectors);
                assert.equal('object', typeof data.response.inputs);
                assert.equal('cloud', data.response.kind);
                assert.equal(false, data.response.managed_by_tower);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('MyCloud', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('credential_type', data.response.type);
                assert.equal('/api/v2/credential_types/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              credentialTypesId = data.response.id;
              saveMockData('CredentialTypes', 'credentialTypesCredentialTypesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialTypesCredentialTypesCredentialTypesCredentialsCreateBodyParam = {
      credential_type: 6,
      name: 'string'
    };
    describe('#credentialTypesCredentialTypesCredentialsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialTypesCredentialTypesCredentialsCreate(credentialTypesId, credentialTypesCredentialTypesCredentialTypesCredentialsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CredentialTypes', 'credentialTypesCredentialTypesCredentialsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesTestCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialTypesCredentialTypesTestCreate(credentialTypesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CredentialTypes', 'credentialTypesCredentialTypesTestCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.credentialTypesCredentialTypesList(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CredentialTypes', 'credentialTypesCredentialTypesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialTypesCredentialTypesCredentialTypesUpdateBodyParam = {
      kind: 'string',
      name: 'string'
    };
    describe('#credentialTypesCredentialTypesUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialTypesCredentialTypesUpdate(credentialTypesId, null, credentialTypesCredentialTypesCredentialTypesUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CredentialTypes', 'credentialTypesCredentialTypesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialTypesCredentialTypesCredentialTypesPartialUpdateBodyParam = {};
    describe('#credentialTypesCredentialTypesPartialUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.credentialTypesCredentialTypesPartialUpdate(credentialTypesId, null, credentialTypesCredentialTypesCredentialTypesPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CredentialTypes', 'credentialTypesCredentialTypesPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesRead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.credentialTypesCredentialTypesRead(credentialTypesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal('object', typeof data.response.injectors);
                assert.equal('object', typeof data.response.inputs);
                assert.equal('cloud', data.response.kind);
                assert.equal(false, data.response.managed_by_tower);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('Some Other Name', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('credential_type', data.response.type);
                assert.equal('/api/v2/credential_types/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CredentialTypes', 'credentialTypesCredentialTypesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialTypesCredentialTypesActivityStreamList(credentialTypesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CredentialTypes', 'credentialTypesCredentialTypesActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesCredentialsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialTypesCredentialTypesCredentialsList(credentialTypesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CredentialTypes', 'credentialTypesCredentialTypesCredentialsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesTestRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialTypesCredentialTypesTestRead(credentialTypesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CredentialTypes', 'credentialTypesCredentialTypesTestRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let credentialsId = 'fakedata';
    const credentialsCredentialsCredentialsCreateBodyParam = {};
    describe('#credentialsCredentialsCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.credentialsCredentialsCreate(credentialsCredentialsCredentialsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.cloud);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal(1, data.response.credential_type);
                assert.equal('', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal('object', typeof data.response.inputs);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('Second Best Credential Ever', data.response.name);
                assert.equal(1, data.response.organization);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('credential', data.response.type);
                assert.equal('/api/v2/credentials/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              credentialsId = data.response.id;
              saveMockData('Credentials', 'credentialsCredentialsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsCredentialsCredentialsCopyCreateBodyParam = {
      name: 'string'
    };
    describe('#credentialsCredentialsCopyCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialsCredentialsCopyCreate(credentialsId, credentialsCredentialsCredentialsCopyCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'credentialsCredentialsCopyCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsCredentialsCredentialsInputSourcesCreateBodyParam = {};
    describe('#credentialsCredentialsInputSourcesCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.credentialsCredentialsInputSourcesCreate(credentialsId, credentialsCredentialsCredentialsInputSourcesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal('vault_password', data.response.input_field_name);
                assert.equal('object', typeof data.response.metadata);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('object', typeof data.response.related);
                assert.equal(2, data.response.source_credential);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(1, data.response.target_credential);
                assert.equal('credential_input_source', data.response.type);
                assert.equal('/api/v2/credential_input_sources/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'credentialsCredentialsInputSourcesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsTestCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialsCredentialsTestCreate(credentialsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'credentialsCredentialsTestCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.credentialsCredentialsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'credentialsCredentialsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsCredentialsCredentialsUpdateBodyParam = {};
    describe('#credentialsCredentialsUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.credentialsCredentialsUpdate(credentialsId, null, credentialsCredentialsCredentialsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'credentialsCredentialsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const credentialsCredentialsCredentialsPartialUpdateBodyParam = {};
    describe('#credentialsCredentialsPartialUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.credentialsCredentialsPartialUpdate(credentialsId, null, credentialsCredentialsCredentialsPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'credentialsCredentialsPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsRead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.credentialsCredentialsRead(credentialsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.cloud);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal(1, data.response.credential_type);
                assert.equal('', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal('object', typeof data.response.inputs);
                assert.equal('ssh', data.response.kind);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('Some name', data.response.name);
                assert.equal(1, data.response.organization);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('credential', data.response.type);
                assert.equal('/api/v2/credentials/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'credentialsCredentialsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsAccessListList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialsCredentialsAccessListList(credentialsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'credentialsCredentialsAccessListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialsCredentialsActivityStreamList(credentialsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'credentialsCredentialsActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsCopyList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialsCredentialsCopyList(credentialsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'credentialsCredentialsCopyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsInputSourcesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.credentialsCredentialsInputSourcesList(credentialsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'credentialsCredentialsInputSourcesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsObjectRolesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialsCredentialsObjectRolesList(credentialsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'credentialsCredentialsObjectRolesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsOwnerTeamsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialsCredentialsOwnerTeamsList(credentialsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'credentialsCredentialsOwnerTeamsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsOwnerUsersList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialsCredentialsOwnerUsersList(credentialsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'credentialsCredentialsOwnerUsersList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsTestRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialsCredentialsTestRead(credentialsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'credentialsCredentialsTestRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dashboardDashboardList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dashboardDashboardList((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboard', 'dashboardDashboardList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dashboardDashboardGraphsJobsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dashboardDashboardGraphsJobsList((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Dashboard', 'dashboardDashboardGraphsJobsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupsGroupsGroupsCreateBodyParam = {
      inventory: 5,
      name: 'string'
    };
    describe('#groupsGroupsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsCreate(groupsGroupsGroupsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupsId = 'fakedata';
    const groupsGroupsGroupsAdHocCommandsCreateBodyParam = {
      become_enabled: 'string',
      credential: 5,
      diff_mode: 'string',
      extra_vars: 'string',
      forks: 5,
      inventory: 1,
      job_type: 'string',
      limit: 'string',
      module_args: 'string',
      module_name: 'string',
      verbosity: 'string'
    };
    describe('#groupsGroupsAdHocCommandsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsAdHocCommandsCreate(groupsId, groupsGroupsGroupsAdHocCommandsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsAdHocCommandsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupsGroupsGroupsChildrenCreateBodyParam = {};
    describe('#groupsGroupsChildrenCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.groupsGroupsChildrenCreate(groupsId, groupsGroupsGroupsChildrenCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('Hello world', data.response.description);
                assert.equal(0, data.response.groups_with_active_failures);
                assert.equal(false, data.response.has_active_failures);
                assert.equal(false, data.response.has_inventory_sources);
                assert.equal(0, data.response.hosts_with_active_failures);
                assert.equal(2, data.response.id);
                assert.equal(1, data.response.inventory);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('New name', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(0, data.response.total_groups);
                assert.equal(0, data.response.total_hosts);
                assert.equal('group', data.response.type);
                assert.equal('/api/v2/groups/2/', data.response.url);
                assert.equal('', data.response.variables);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsChildrenCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupsGroupsGroupsHostsCreateBodyParam = {};
    describe('#groupsGroupsHostsCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.groupsGroupsHostsCreate(groupsId, groupsGroupsGroupsHostsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('Hello world', data.response.description);
                assert.equal(true, data.response.enabled);
                assert.equal(false, data.response.has_active_failures);
                assert.equal(false, data.response.has_inventory_sources);
                assert.equal(1, data.response.id);
                assert.equal('', data.response.instance_id);
                assert.equal(1, data.response.inventory);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('New name', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('host', data.response.type);
                assert.equal('/api/v2/hosts/1/', data.response.url);
                assert.equal('', data.response.variables);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsHostsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupsGroupsGroupsUpdateBodyParam = {};
    describe('#groupsGroupsUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.groupsGroupsUpdate(groupsId, null, groupsGroupsGroupsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupsGroupsGroupsPartialUpdateBodyParam = {
      description: 'string',
      inventory: 7,
      name: 'string',
      variables: 'string'
    };
    describe('#groupsGroupsPartialUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsPartialUpdate(groupsId, null, groupsGroupsGroupsPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsRead(groupsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsActivityStreamList(groupsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsAdHocCommandsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsAdHocCommandsList(groupsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsAdHocCommandsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsAllHostsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsAllHostsList(groupsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsAllHostsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsChildrenList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsChildrenList(groupsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsChildrenList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsHostsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsHostsList(groupsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsHostsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsInventorySourcesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsInventorySourcesList(groupsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsInventorySourcesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsJobEventsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsJobEventsList(groupsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsJobEventsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsJobHostSummariesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsJobHostSummariesList(groupsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsJobHostSummariesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsPotentialChildrenList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsPotentialChildrenList(groupsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsPotentialChildrenList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupsGroupsGroupsVariableDataUpdateBodyParam = {
      variables: 'string'
    };
    describe('#groupsGroupsVariableDataUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsVariableDataUpdate(groupsId, null, groupsGroupsGroupsVariableDataUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsVariableDataUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const groupsGroupsGroupsVariableDataPartialUpdateBodyParam = {
      variables: 'string'
    };
    describe('#groupsGroupsVariableDataPartialUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsVariableDataPartialUpdate(groupsId, null, groupsGroupsGroupsVariableDataPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsVariableDataPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsVariableDataRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsVariableDataRead(groupsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsVariableDataRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hostsHostsHostsCreateBodyParam = {
      inventory: 5,
      name: 'string'
    };
    describe('#hostsHostsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsCreate(hostsHostsHostsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hostsId = 'fakedata';
    const hostsHostsHostsAdHocCommandsCreateBodyParam = {
      become_enabled: 'string',
      credential: 9,
      diff_mode: 'string',
      extra_vars: 'string',
      forks: 5,
      inventory: 8,
      job_type: 'string',
      limit: 'string',
      module_args: 'string',
      module_name: 'string',
      verbosity: 'string'
    };
    describe('#hostsHostsAdHocCommandsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsAdHocCommandsCreate(hostsId, hostsHostsHostsAdHocCommandsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsAdHocCommandsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hostsHostsHostsGroupsCreateBodyParam = {};
    describe('#hostsHostsGroupsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsGroupsCreate(hostsId, hostsHostsHostsGroupsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsGroupsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.hostsHostsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(20, data.response.count);
                assert.equal('/api/v2/hosts/?page=3&page_size=5', data.response.next);
                assert.equal('/api/v2/hosts/?page=1&page_size=5', data.response.previous);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hostsHostsHostsUpdateBodyParam = {};
    describe('#hostsHostsUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.hostsHostsUpdate(hostsId, null, hostsHostsHostsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hostsHostsHostsPartialUpdateBodyParam = {
      description: 'string',
      enabled: 'string',
      instance_id: 'string',
      inventory: 8,
      name: 'string',
      variables: 'string'
    };
    describe('#hostsHostsPartialUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsPartialUpdate(hostsId, null, hostsHostsHostsPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsRead(hostsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsActivityStreamList(hostsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsAdHocCommandEventsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsAdHocCommandEventsList(hostsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsAdHocCommandEventsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsAdHocCommandsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsAdHocCommandsList(hostsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsAdHocCommandsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsAllGroupsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsAllGroupsList(hostsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsAllGroupsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsAnsibleFactsRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsAnsibleFactsRead(hostsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsAnsibleFactsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsGroupsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsGroupsList(hostsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsGroupsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsInsightsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsInsightsList(hostsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsInsightsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsInventorySourcesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsInventorySourcesList(hostsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsInventorySourcesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsJobEventsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsJobEventsList(hostsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsJobEventsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsJobHostSummariesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsJobHostSummariesList(hostsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsJobHostSummariesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsSmartInventoriesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsSmartInventoriesList(hostsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsSmartInventoriesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hostsHostsHostsVariableDataUpdateBodyParam = {
      variables: 'string'
    };
    describe('#hostsHostsVariableDataUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsVariableDataUpdate(hostsId, null, hostsHostsHostsVariableDataUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsVariableDataUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hostsHostsHostsVariableDataPartialUpdateBodyParam = {
      variables: 'string'
    };
    describe('#hostsHostsVariableDataPartialUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsVariableDataPartialUpdate(hostsId, null, hostsHostsHostsVariableDataPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsVariableDataPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsVariableDataRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsVariableDataRead(hostsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsVariableDataRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instanceGroupsInstanceGroupsInstanceGroupsCreateBodyParam = {
      name: 'string'
    };
    describe('#instanceGroupsInstanceGroupsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instanceGroupsInstanceGroupsCreate(instanceGroupsInstanceGroupsInstanceGroupsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InstanceGroups', 'instanceGroupsInstanceGroupsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instanceGroupsId = 'fakedata';
    const instanceGroupsInstanceGroupsInstanceGroupsInstancesCreateBodyParam = {
      capacity_adjustment: 6,
      enabled: 'string',
      managed_by_policy: 'string'
    };
    describe('#instanceGroupsInstanceGroupsInstancesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instanceGroupsInstanceGroupsInstancesCreate(instanceGroupsId, instanceGroupsInstanceGroupsInstanceGroupsInstancesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InstanceGroups', 'instanceGroupsInstanceGroupsInstancesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instanceGroupsInstanceGroupsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instanceGroupsInstanceGroupsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InstanceGroups', 'instanceGroupsInstanceGroupsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instanceGroupsInstanceGroupsInstanceGroupsUpdateBodyParam = {
      name: 'string'
    };
    describe('#instanceGroupsInstanceGroupsUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instanceGroupsInstanceGroupsUpdate(instanceGroupsId, null, instanceGroupsInstanceGroupsInstanceGroupsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InstanceGroups', 'instanceGroupsInstanceGroupsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instanceGroupsInstanceGroupsInstanceGroupsPartialUpdateBodyParam = {};
    describe('#instanceGroupsInstanceGroupsPartialUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.instanceGroupsInstanceGroupsPartialUpdate(instanceGroupsId, null, instanceGroupsInstanceGroupsInstanceGroupsPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InstanceGroups', 'instanceGroupsInstanceGroupsPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instanceGroupsInstanceGroupsRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instanceGroupsInstanceGroupsRead(instanceGroupsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InstanceGroups', 'instanceGroupsInstanceGroupsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instanceGroupsInstanceGroupsInstancesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instanceGroupsInstanceGroupsInstancesList(instanceGroupsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InstanceGroups', 'instanceGroupsInstanceGroupsInstancesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instanceGroupsInstanceGroupsJobsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instanceGroupsInstanceGroupsJobsList(instanceGroupsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InstanceGroups', 'instanceGroupsInstanceGroupsJobsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instancesId = 'fakedata';
    const instancesInstancesInstancesInstanceGroupsCreateBodyParam = {
      name: 'string'
    };
    describe('#instancesInstancesInstanceGroupsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instancesInstancesInstanceGroupsCreate(instancesId, instancesInstancesInstancesInstanceGroupsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instances', 'instancesInstancesInstanceGroupsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instancesInstancesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instancesInstancesList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instances', 'instancesInstancesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instancesInstancesInstancesUpdateBodyParam = {
      capacity_adjustment: 4,
      enabled: 'string',
      managed_by_policy: 'string'
    };
    describe('#instancesInstancesUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instancesInstancesUpdate(instancesId, null, instancesInstancesInstancesUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instances', 'instancesInstancesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const instancesInstancesInstancesPartialUpdateBodyParam = {
      capacity_adjustment: 4,
      enabled: 'string',
      managed_by_policy: 'string'
    };
    describe('#instancesInstancesPartialUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instancesInstancesPartialUpdate(instancesId, null, instancesInstancesInstancesPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instances', 'instancesInstancesPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instancesInstancesRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instancesInstancesRead(instancesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instances', 'instancesInstancesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instancesInstancesInstanceGroupsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instancesInstancesInstanceGroupsList(instancesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instances', 'instancesInstancesInstanceGroupsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instancesInstancesJobsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instancesInstancesJobsList(instancesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Instances', 'instancesInstancesJobsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let inventoriesId = 'fakedata';
    const inventoriesInventoriesInventoriesCreateBodyParam = {};
    describe('#inventoriesInventoriesCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoriesInventoriesCreate(inventoriesInventoriesInventoriesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(0, data.response.groups_with_active_failures);
                assert.equal(false, data.response.has_active_failures);
                assert.equal(false, data.response.has_inventory_sources);
                assert.equal('ansible_facts__ansible_distribution__exact="CentOS"', data.response.host_filter);
                assert.equal(0, data.response.hosts_with_active_failures);
                assert.equal(1, data.response.id);
                assert.equal(0, data.response.inventory_sources_with_failures);
                assert.equal('smart', data.response.kind);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('smart inventory', data.response.name);
                assert.equal(1, data.response.organization);
                assert.equal(false, data.response.pending_deletion);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(0, data.response.total_groups);
                assert.equal(0, data.response.total_hosts);
                assert.equal(0, data.response.total_inventory_sources);
                assert.equal('inventory', data.response.type);
                assert.equal('/api/v2/inventories/1/', data.response.url);
                assert.equal('', data.response.variables);
              } else {
                runCommonAsserts(data, error);
              }
              inventoriesId = data.response.id;
              saveMockData('Inventories', 'inventoriesInventoriesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoriesInventoriesInventoriesAdHocCommandsCreateBodyParam = {};
    describe('#inventoriesInventoriesAdHocCommandsCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoriesInventoriesAdHocCommandsCreate(inventoriesId, inventoriesInventoriesInventoriesAdHocCommandsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.become_enabled);
                assert.equal('', data.response.controller_node);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal(1, data.response.credential);
                assert.equal(false, data.response.diff_mode);
                assert.equal(0, data.response.elapsed);
                assert.equal('', data.response.execution_node);
                assert.equal('', data.response.extra_vars);
                assert.equal(false, data.response.failed);
                assert.equal(0, data.response.forks);
                assert.equal(2, data.response.id);
                assert.equal(1, data.response.inventory);
                assert.equal('', data.response.job_explanation);
                assert.equal('run', data.response.job_type);
                assert.equal('manual', data.response.launch_type);
                assert.equal('', data.response.limit);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('uptime', data.response.module_args);
                assert.equal('command', data.response.module_name);
                assert.equal('command', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('new', data.response.status);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('ad_hoc_command', data.response.type);
                assert.equal('/api/v2/ad_hoc_commands/2/', data.response.url);
                assert.equal(0, data.response.verbosity);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesAdHocCommandsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoriesInventoriesInventoriesCopyCreateBodyParam = {
      name: 'string'
    };
    describe('#inventoriesInventoriesCopyCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoriesInventoriesCopyCreate(inventoriesId, inventoriesInventoriesInventoriesCopyCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesCopyCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoriesInventoriesInventoriesGroupsCreateBodyParam = {};
    describe('#inventoriesInventoriesGroupsCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoriesInventoriesGroupsCreate(inventoriesId, inventoriesInventoriesInventoriesGroupsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('Hello world', data.response.description);
                assert.equal(0, data.response.groups_with_active_failures);
                assert.equal(false, data.response.has_active_failures);
                assert.equal(false, data.response.has_inventory_sources);
                assert.equal(0, data.response.hosts_with_active_failures);
                assert.equal(1, data.response.id);
                assert.equal(1, data.response.inventory);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('New name', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(0, data.response.total_groups);
                assert.equal(0, data.response.total_hosts);
                assert.equal('group', data.response.type);
                assert.equal('/api/v2/groups/1/', data.response.url);
                assert.equal('', data.response.variables);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesGroupsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoriesInventoriesInventoriesHostsCreateBodyParam = {};
    describe('#inventoriesInventoriesHostsCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoriesInventoriesHostsCreate(inventoriesId, inventoriesInventoriesInventoriesHostsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('Hello world', data.response.description);
                assert.equal(true, data.response.enabled);
                assert.equal(false, data.response.has_active_failures);
                assert.equal(false, data.response.has_inventory_sources);
                assert.equal(4, data.response.id);
                assert.equal('', data.response.instance_id);
                assert.equal(1, data.response.inventory);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('New name', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('host', data.response.type);
                assert.equal('/api/v2/hosts/4/', data.response.url);
                assert.equal('', data.response.variables);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesHostsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoriesInventoriesInventoriesInstanceGroupsCreateBodyParam = {};
    describe('#inventoriesInventoriesInstanceGroupsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoriesInventoriesInstanceGroupsCreate(inventoriesId, inventoriesInventoriesInventoriesInstanceGroupsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesInstanceGroupsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoriesInventoriesInventoriesInventorySourcesCreateBodyParam = {};
    describe('#inventoriesInventoriesInventorySourcesCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoriesInventoriesInventorySourcesCreate(inventoriesId, inventoriesInventoriesInventoriesInventorySourcesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal('', data.response.group_by);
                assert.equal(3, data.response.id);
                assert.equal('', data.response.instance_filters);
                assert.equal(1, data.response.inventory);
                assert.equal(false, data.response.last_job_failed);
                assert.equal(false, data.response.last_update_failed);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('new inv src', data.response.name);
                assert.equal(false, data.response.overwrite);
                assert.equal(true, data.response.overwrite_vars);
                assert.equal('object', typeof data.response.related);
                assert.equal('scm', data.response.source);
                assert.equal('', data.response.source_path);
                assert.equal('', data.response.source_regions);
                assert.equal('', data.response.source_vars);
                assert.equal('never updated', data.response.status);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(0, data.response.timeout);
                assert.equal('inventory_source', data.response.type);
                assert.equal(0, data.response.update_cache_timeout);
                assert.equal(false, data.response.update_on_launch);
                assert.equal(false, data.response.update_on_project_update);
                assert.equal('/api/v2/inventory_sources/3/', data.response.url);
                assert.equal(1, data.response.verbosity);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesInventorySourcesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoriesInventoriesInventoriesRootGroupsCreateBodyParam = {
      inventory: 7,
      name: 'string'
    };
    describe('#inventoriesInventoriesRootGroupsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoriesInventoriesRootGroupsCreate(inventoriesId, inventoriesInventoriesInventoriesRootGroupsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesRootGroupsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesUpdateInventorySourcesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoriesInventoriesUpdateInventorySourcesCreate(inventoriesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesUpdateInventorySourcesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoriesInventoriesList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoriesInventoriesInventoriesUpdateBodyParam = {};
    describe('#inventoriesInventoriesUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoriesInventoriesUpdate(inventoriesId, null, inventoriesInventoriesInventoriesUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoriesInventoriesInventoriesPartialUpdateBodyParam = {};
    describe('#inventoriesInventoriesPartialUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoriesInventoriesPartialUpdate(inventoriesId, null, inventoriesInventoriesInventoriesPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesRead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoriesInventoriesRead(inventoriesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(0, data.response.groups_with_active_failures);
                assert.equal(false, data.response.has_active_failures);
                assert.equal(true, data.response.has_inventory_sources);
                assert.equal(0, data.response.hosts_with_active_failures);
                assert.equal(1, data.response.id);
                assert.equal(0, data.response.inventory_sources_with_failures);
                assert.equal('', data.response.kind);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('test-inv', data.response.name);
                assert.equal(1, data.response.organization);
                assert.equal(false, data.response.pending_deletion);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(0, data.response.total_groups);
                assert.equal(0, data.response.total_hosts);
                assert.equal(1, data.response.total_inventory_sources);
                assert.equal('inventory', data.response.type);
                assert.equal('/api/v2/inventories/1/', data.response.url);
                assert.equal('', data.response.variables);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesAccessListList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoriesInventoriesAccessListList(inventoriesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesAccessListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoriesInventoriesActivityStreamList(inventoriesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesAdHocCommandsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoriesInventoriesAdHocCommandsList(inventoriesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesAdHocCommandsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesCopyList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoriesInventoriesCopyList(inventoriesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesCopyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesGroupsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoriesInventoriesGroupsList(inventoriesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(0, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesGroupsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesHostsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoriesInventoriesHostsList(inventoriesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesHostsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesInstanceGroupsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoriesInventoriesInstanceGroupsList(inventoriesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesInstanceGroupsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesInventorySourcesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoriesInventoriesInventorySourcesList(inventoriesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(0, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesInventorySourcesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesJobTemplatesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoriesInventoriesJobTemplatesList(inventoriesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesJobTemplatesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesObjectRolesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoriesInventoriesObjectRolesList(inventoriesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesObjectRolesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesRootGroupsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoriesInventoriesRootGroupsList(inventoriesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesRootGroupsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesScriptRead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoriesInventoriesScriptRead(inventoriesId, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.all);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesScriptRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesTreeRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoriesInventoriesTreeRead(inventoriesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesTreeRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesUpdateInventorySourcesRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoriesInventoriesUpdateInventorySourcesRead(inventoriesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesUpdateInventorySourcesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoriesInventoriesInventoriesVariableDataUpdateBodyParam = {};
    describe('#inventoriesInventoriesVariableDataUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoriesInventoriesVariableDataUpdate(inventoriesId, null, inventoriesInventoriesInventoriesVariableDataUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesVariableDataUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoriesInventoriesInventoriesVariableDataPartialUpdateBodyParam = {};
    describe('#inventoriesInventoriesVariableDataPartialUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoriesInventoriesVariableDataPartialUpdate(inventoriesId, null, inventoriesInventoriesInventoriesVariableDataPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesVariableDataPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesVariableDataRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoriesInventoriesVariableDataRead(inventoriesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesVariableDataRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customInventoryScriptsCustomInventoryScriptsInventoryScriptsCreateBodyParam = {
      name: 'string',
      organization: 6,
      script: 'string'
    };
    describe('#customInventoryScriptsInventoryScriptsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.customInventoryScriptsInventoryScriptsCreate(customInventoryScriptsCustomInventoryScriptsInventoryScriptsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomInventoryScripts', 'customInventoryScriptsInventoryScriptsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customInventoryScriptsId = 'fakedata';
    const customInventoryScriptsCustomInventoryScriptsInventoryScriptsCopyCreateBodyParam = {
      name: 'string'
    };
    describe('#customInventoryScriptsInventoryScriptsCopyCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.customInventoryScriptsInventoryScriptsCopyCreate(customInventoryScriptsId, customInventoryScriptsCustomInventoryScriptsInventoryScriptsCopyCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomInventoryScripts', 'customInventoryScriptsInventoryScriptsCopyCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customInventoryScriptsInventoryScriptsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.customInventoryScriptsInventoryScriptsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomInventoryScripts', 'customInventoryScriptsInventoryScriptsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customInventoryScriptsCustomInventoryScriptsInventoryScriptsUpdateBodyParam = {
      name: 'string',
      organization: 1,
      script: 'string'
    };
    describe('#customInventoryScriptsInventoryScriptsUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.customInventoryScriptsInventoryScriptsUpdate(customInventoryScriptsId, null, customInventoryScriptsCustomInventoryScriptsInventoryScriptsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomInventoryScripts', 'customInventoryScriptsInventoryScriptsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const customInventoryScriptsCustomInventoryScriptsInventoryScriptsPartialUpdateBodyParam = {
      description: 'string',
      name: 'string',
      organization: 9,
      script: 'string'
    };
    describe('#customInventoryScriptsInventoryScriptsPartialUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.customInventoryScriptsInventoryScriptsPartialUpdate(customInventoryScriptsId, null, customInventoryScriptsCustomInventoryScriptsInventoryScriptsPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomInventoryScripts', 'customInventoryScriptsInventoryScriptsPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customInventoryScriptsInventoryScriptsRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.customInventoryScriptsInventoryScriptsRead(customInventoryScriptsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomInventoryScripts', 'customInventoryScriptsInventoryScriptsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customInventoryScriptsInventoryScriptsCopyList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.customInventoryScriptsInventoryScriptsCopyList(customInventoryScriptsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomInventoryScripts', 'customInventoryScriptsInventoryScriptsCopyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customInventoryScriptsInventoryScriptsObjectRolesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.customInventoryScriptsInventoryScriptsObjectRolesList(customInventoryScriptsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomInventoryScripts', 'customInventoryScriptsInventoryScriptsObjectRolesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let inventorySourcesId = 'fakedata';
    const inventorySourcesInventorySourcesInventorySourcesCreateBodyParam = {};
    describe('#inventorySourcesInventorySourcesCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventorySourcesInventorySourcesCreate(inventorySourcesInventorySourcesInventorySourcesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal(1, data.response.credential);
                assert.equal('', data.response.description);
                assert.equal('', data.response.group_by);
                assert.equal(2, data.response.id);
                assert.equal('', data.response.instance_filters);
                assert.equal(1, data.response.inventory);
                assert.equal(false, data.response.last_job_failed);
                assert.equal(false, data.response.last_update_failed);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('fobar', data.response.name);
                assert.equal(false, data.response.overwrite);
                assert.equal(false, data.response.overwrite_vars);
                assert.equal('object', typeof data.response.related);
                assert.equal('scm', data.response.source);
                assert.equal('', data.response.source_path);
                assert.equal(1, data.response.source_project);
                assert.equal('', data.response.source_regions);
                assert.equal('', data.response.source_vars);
                assert.equal('never updated', data.response.status);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(0, data.response.timeout);
                assert.equal('inventory_source', data.response.type);
                assert.equal(0, data.response.update_cache_timeout);
                assert.equal(false, data.response.update_on_launch);
                assert.equal(false, data.response.update_on_project_update);
                assert.equal('/api/v2/inventory_sources/2/', data.response.url);
                assert.equal(1, data.response.verbosity);
              } else {
                runCommonAsserts(data, error);
              }
              inventorySourcesId = data.response.id;
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventorySourcesInventorySourcesInventorySourcesCredentialsCreateBodyParam = {
      credential_type: 3,
      name: 'string'
    };
    describe('#inventorySourcesInventorySourcesCredentialsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesCredentialsCreate(inventorySourcesId, inventorySourcesInventorySourcesInventorySourcesCredentialsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesCredentialsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventorySourcesInventorySourcesInventorySourcesNotificationTemplatesErrorCreateBodyParam = {
      name: 'string',
      notification_type: 'string',
      organization: 8
    };
    describe('#inventorySourcesInventorySourcesNotificationTemplatesErrorCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesNotificationTemplatesErrorCreate(inventorySourcesId, inventorySourcesInventorySourcesInventorySourcesNotificationTemplatesErrorCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesNotificationTemplatesErrorCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventorySourcesInventorySourcesInventorySourcesNotificationTemplatesStartedCreateBodyParam = {};
    describe('#inventorySourcesInventorySourcesNotificationTemplatesStartedCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesNotificationTemplatesStartedCreate(inventorySourcesId, inventorySourcesInventorySourcesInventorySourcesNotificationTemplatesStartedCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesNotificationTemplatesStartedCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventorySourcesInventorySourcesInventorySourcesNotificationTemplatesSuccessCreateBodyParam = {
      name: 'string',
      notification_type: 'string',
      organization: 9
    };
    describe('#inventorySourcesInventorySourcesNotificationTemplatesSuccessCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesNotificationTemplatesSuccessCreate(inventorySourcesId, inventorySourcesInventorySourcesInventorySourcesNotificationTemplatesSuccessCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesNotificationTemplatesSuccessCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventorySourcesInventorySourcesInventorySourcesSchedulesCreateBodyParam = {
      name: 'string',
      rrule: 'string',
      unified_job_template: 3
    };
    describe('#inventorySourcesInventorySourcesSchedulesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesSchedulesCreate(inventorySourcesId, inventorySourcesInventorySourcesInventorySourcesSchedulesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesSchedulesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesUpdateCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesUpdateCreate(inventorySourcesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesUpdateCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventorySourcesInventorySourcesInventorySourcesUpdate0BodyParam = {
      name: 'string'
    };
    describe('#inventorySourcesInventorySourcesUpdate0 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesUpdate0(inventorySourcesId, null, inventorySourcesInventorySourcesInventorySourcesUpdate0BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesUpdate0', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventorySourcesInventorySourcesInventorySourcesPartialUpdateBodyParam = {};
    describe('#inventorySourcesInventorySourcesPartialUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventorySourcesInventorySourcesPartialUpdate(inventorySourcesId, null, inventorySourcesInventorySourcesInventorySourcesPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesRead(inventorySourcesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesActivityStreamList(inventorySourcesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesCredentialsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesCredentialsList(inventorySourcesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesCredentialsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesGroupsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesGroupsList(inventorySourcesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesGroupsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesHostsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesHostsList(inventorySourcesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesHostsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesInventoryUpdatesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesInventoryUpdatesList(inventorySourcesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesInventoryUpdatesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesNotificationTemplatesErrorList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesNotificationTemplatesErrorList(inventorySourcesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesNotificationTemplatesErrorList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesNotificationTemplatesStartedList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventorySourcesInventorySourcesNotificationTemplatesStartedList(inventorySourcesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesNotificationTemplatesStartedList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesNotificationTemplatesSuccessList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesNotificationTemplatesSuccessList(inventorySourcesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesNotificationTemplatesSuccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesSchedulesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesSchedulesList(inventorySourcesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesSchedulesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesUpdateRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesUpdateRead(inventorySourcesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesUpdateRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const inventoryUpdatesId = 'fakedata';
    describe('#inventoryUpdatesInventoryUpdatesCancelCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesCancelCreate(inventoryUpdatesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventoryUpdates', 'inventoryUpdatesInventoryUpdatesCancelCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventoryUpdates', 'inventoryUpdatesInventoryUpdatesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesRead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesRead(inventoryUpdatesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(0, data.response.elapsed);
                assert.equal(false, data.response.event_processing_finished);
                assert.equal('', data.response.execution_node);
                assert.equal(false, data.response.failed);
                assert.equal('', data.response.group_by);
                assert.equal(3, data.response.id);
                assert.equal('', data.response.instance_filters);
                assert.equal(3, data.response.inventory_source);
                assert.equal('', data.response.job_args);
                assert.equal('', data.response.job_cwd);
                assert.equal('object', typeof data.response.job_env);
                assert.equal('', data.response.job_explanation);
                assert.equal('manual', data.response.launch_type);
                assert.equal(false, data.response.license_error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('', data.response.name);
                assert.equal(false, data.response.org_host_limit_error);
                assert.equal(false, data.response.overwrite);
                assert.equal(false, data.response.overwrite_vars);
                assert.equal('object', typeof data.response.related);
                assert.equal('', data.response.result_traceback);
                assert.equal('', data.response.source);
                assert.equal('', data.response.source_path);
                assert.equal('', data.response.source_regions);
                assert.equal('', data.response.source_vars);
                assert.equal('new', data.response.status);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(0, data.response.timeout);
                assert.equal('inventory_update', data.response.type);
                assert.equal(3, data.response.unified_job_template);
                assert.equal('/api/v2/inventory_updates/3/', data.response.url);
                assert.equal(1, data.response.verbosity);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventoryUpdates', 'inventoryUpdatesInventoryUpdatesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesCancelRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesCancelRead(inventoryUpdatesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventoryUpdates', 'inventoryUpdatesInventoryUpdatesCancelRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesCredentialsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesCredentialsList(inventoryUpdatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventoryUpdates', 'inventoryUpdatesInventoryUpdatesCredentialsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesEventsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesEventsList(inventoryUpdatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventoryUpdates', 'inventoryUpdatesInventoryUpdatesEventsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesNotificationsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesNotificationsList(inventoryUpdatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventoryUpdates', 'inventoryUpdatesInventoryUpdatesNotificationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesStdoutRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesStdoutRead(inventoryUpdatesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventoryUpdates', 'inventoryUpdatesInventoryUpdatesStdoutRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobEventsJobEventsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobEventsJobEventsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobEvents', 'jobEventsJobEventsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobEventsId = 'fakedata';
    describe('#jobEventsJobEventsRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobEventsJobEventsRead(jobEventsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobEvents', 'jobEventsJobEventsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobEventsJobEventsChildrenList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobEventsJobEventsChildrenList(jobEventsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobEvents', 'jobEventsJobEventsChildrenList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobEventsJobEventsHostsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobEventsJobEventsHostsList(jobEventsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobEvents', 'jobEventsJobEventsHostsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobHostSummariesId = 'fakedata';
    describe('#jobHostSummariesJobHostSummariesRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobHostSummariesJobHostSummariesRead(jobHostSummariesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobHostSummaries', 'jobHostSummariesJobHostSummariesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let jobTemplatesId = 'fakedata';
    const jobTemplatesJobTemplatesJobTemplatesCreateBodyParam = {};
    describe('#jobTemplatesJobTemplatesCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobTemplatesJobTemplatesCreate(jobTemplatesJobTemplatesJobTemplatesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.allow_simultaneous);
                assert.equal(false, data.response.ask_credential_on_launch);
                assert.equal(false, data.response.ask_diff_mode_on_launch);
                assert.equal(false, data.response.ask_inventory_on_launch);
                assert.equal(false, data.response.ask_job_type_on_launch);
                assert.equal(false, data.response.ask_limit_on_launch);
                assert.equal(false, data.response.ask_scm_branch_on_launch);
                assert.equal(false, data.response.ask_skip_tags_on_launch);
                assert.equal(false, data.response.ask_tags_on_launch);
                assert.equal(false, data.response.ask_variables_on_launch);
                assert.equal(false, data.response.ask_verbosity_on_launch);
                assert.equal(false, data.response.become_enabled);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(false, data.response.diff_mode);
                assert.equal('', data.response.extra_vars);
                assert.equal(false, data.response.force_handlers);
                assert.equal(0, data.response.forks);
                assert.equal('', data.response.host_config_key);
                assert.equal(2, data.response.id);
                assert.equal(1, data.response.inventory);
                assert.equal(1, data.response.job_slice_count);
                assert.equal('', data.response.job_tags);
                assert.equal('run', data.response.job_type);
                assert.equal(false, data.response.last_job_failed);
                assert.equal('', data.response.limit);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('Some name', data.response.name);
                assert.equal('helloworld.yml', data.response.playbook);
                assert.equal(1, data.response.project);
                assert.equal('object', typeof data.response.related);
                assert.equal('', data.response.scm_branch);
                assert.equal('', data.response.skip_tags);
                assert.equal('', data.response.start_at_task);
                assert.equal('never updated', data.response.status);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(false, data.response.survey_enabled);
                assert.equal(0, data.response.timeout);
                assert.equal('job_template', data.response.type);
                assert.equal('/api/v2/job_templates/2/', data.response.url);
                assert.equal(false, data.response.use_fact_cache);
                assert.equal(0, data.response.verbosity);
              } else {
                runCommonAsserts(data, error);
              }
              jobTemplatesId = data.response.id;
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobTemplatesJobTemplatesJobTemplatesCallbackCreateBodyParam = {};
    describe('#jobTemplatesJobTemplatesCallbackCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesCallbackCreate(jobTemplatesId, jobTemplatesJobTemplatesJobTemplatesCallbackCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesCallbackCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobTemplatesJobTemplatesJobTemplatesCopyCreateBodyParam = {
      name: 'string'
    };
    describe('#jobTemplatesJobTemplatesCopyCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesCopyCreate(jobTemplatesId, jobTemplatesJobTemplatesJobTemplatesCopyCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesCopyCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobTemplatesJobTemplatesJobTemplatesCredentialsCreateBodyParam = {};
    describe('#jobTemplatesJobTemplatesCredentialsCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobTemplatesJobTemplatesCredentialsCreate(jobTemplatesId, jobTemplatesJobTemplatesJobTemplatesCredentialsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.cloud);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal(1, data.response.credential_type);
                assert.equal('', data.response.description);
                assert.equal(2, data.response.id);
                assert.equal('object', typeof data.response.inputs);
                assert.equal('ssh', data.response.kind);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('First Cred', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('credential', data.response.type);
                assert.equal('/api/v2/credentials/2/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesCredentialsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobTemplatesJobTemplatesJobTemplatesExtraCredentialsCreateBodyParam = {};
    describe('#jobTemplatesJobTemplatesExtraCredentialsCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobTemplatesJobTemplatesExtraCredentialsCreate(jobTemplatesId, jobTemplatesJobTemplatesJobTemplatesExtraCredentialsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.cloud);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal(1, data.response.credential_type);
                assert.equal('', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal('object', typeof data.response.inputs);
                assert.equal('aws', data.response.kind);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('My Cred', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('credential', data.response.type);
                assert.equal('/api/v2/credentials/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesExtraCredentialsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobTemplatesJobTemplatesJobTemplatesInstanceGroupsCreateBodyParam = {};
    describe('#jobTemplatesJobTemplatesInstanceGroupsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesInstanceGroupsCreate(jobTemplatesId, jobTemplatesJobTemplatesJobTemplatesInstanceGroupsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesInstanceGroupsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobTemplatesJobTemplatesJobTemplatesLabelsCreateBodyParam = {
      name: 'string',
      organization: 3
    };
    describe('#jobTemplatesJobTemplatesLabelsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesLabelsCreate(jobTemplatesId, jobTemplatesJobTemplatesJobTemplatesLabelsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesLabelsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobTemplatesJobTemplatesJobTemplatesLaunchCreateBodyParam = {};
    describe('#jobTemplatesJobTemplatesLaunchCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobTemplatesJobTemplatesLaunchCreate(jobTemplatesId, jobTemplatesJobTemplatesJobTemplatesLaunchCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response.ignored_fields);
                assert.equal(968, data.response.job);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesLaunchCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobTemplatesJobTemplatesJobTemplatesNotificationTemplatesErrorCreateBodyParam = {
      name: 'string',
      notification_type: 'string',
      organization: 4
    };
    describe('#jobTemplatesJobTemplatesNotificationTemplatesErrorCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesNotificationTemplatesErrorCreate(jobTemplatesId, jobTemplatesJobTemplatesJobTemplatesNotificationTemplatesErrorCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesNotificationTemplatesErrorCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobTemplatesJobTemplatesJobTemplatesNotificationTemplatesStartedCreateBodyParam = {};
    describe('#jobTemplatesJobTemplatesNotificationTemplatesStartedCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesNotificationTemplatesStartedCreate(jobTemplatesId, jobTemplatesJobTemplatesJobTemplatesNotificationTemplatesStartedCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesNotificationTemplatesStartedCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobTemplatesJobTemplatesJobTemplatesNotificationTemplatesSuccessCreateBodyParam = {
      name: 'string',
      notification_type: 'string',
      organization: 4
    };
    describe('#jobTemplatesJobTemplatesNotificationTemplatesSuccessCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesNotificationTemplatesSuccessCreate(jobTemplatesId, jobTemplatesJobTemplatesJobTemplatesNotificationTemplatesSuccessCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesNotificationTemplatesSuccessCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobTemplatesJobTemplatesJobTemplatesSchedulesCreateBodyParam = {};
    describe('#jobTemplatesJobTemplatesSchedulesCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobTemplatesJobTemplatesSchedulesCreate(jobTemplatesId, jobTemplatesJobTemplatesJobTemplatesSchedulesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal('2015-11-17T05:00:00Z', data.response.dtend);
                assert.equal('2015-11-17T05:00:00Z', data.response.dtstart);
                assert.equal(true, data.response.enabled);
                assert.equal('object', typeof data.response.extra_data);
                assert.equal(1, data.response.id);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('test sch', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('DTSTART:20151117T050000Z RRULE:FREQ=DAILY;INTERVAL=1;COUNT=1', data.response.rrule);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('UTC', data.response.timezone);
                assert.equal('schedule', data.response.type);
                assert.equal(2, data.response.unified_job_template);
                assert.equal('', data.response.until);
                assert.equal('/api/v2/schedules/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesSchedulesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobTemplatesJobTemplatesJobTemplatesSliceWorkflowJobsCreateBodyParam = {
      name: 'string'
    };
    describe('#jobTemplatesJobTemplatesSliceWorkflowJobsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesSliceWorkflowJobsCreate(jobTemplatesId, jobTemplatesJobTemplatesJobTemplatesSliceWorkflowJobsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesSliceWorkflowJobsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobTemplatesJobTemplatesJobTemplatesSurveySpecCreateBodyParam = {};
    describe('#jobTemplatesJobTemplatesSurveySpecCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesSurveySpecCreate(jobTemplatesId, jobTemplatesJobTemplatesJobTemplatesSurveySpecCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesSurveySpecCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobTemplatesJobTemplatesJobTemplatesUpdateBodyParam = {};
    describe('#jobTemplatesJobTemplatesUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobTemplatesJobTemplatesUpdate(jobTemplatesId, null, jobTemplatesJobTemplatesJobTemplatesUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobTemplatesJobTemplatesJobTemplatesPartialUpdateBodyParam = {};
    describe('#jobTemplatesJobTemplatesPartialUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobTemplatesJobTemplatesPartialUpdate(jobTemplatesId, null, jobTemplatesJobTemplatesJobTemplatesPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesRead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobTemplatesJobTemplatesRead(jobTemplatesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.allow_simultaneous);
                assert.equal(false, data.response.ask_credential_on_launch);
                assert.equal(false, data.response.ask_diff_mode_on_launch);
                assert.equal(false, data.response.ask_inventory_on_launch);
                assert.equal(false, data.response.ask_job_type_on_launch);
                assert.equal(false, data.response.ask_limit_on_launch);
                assert.equal(false, data.response.ask_scm_branch_on_launch);
                assert.equal(false, data.response.ask_skip_tags_on_launch);
                assert.equal(false, data.response.ask_tags_on_launch);
                assert.equal(false, data.response.ask_variables_on_launch);
                assert.equal(false, data.response.ask_verbosity_on_launch);
                assert.equal(false, data.response.become_enabled);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(false, data.response.diff_mode);
                assert.equal('', data.response.extra_vars);
                assert.equal(false, data.response.force_handlers);
                assert.equal(0, data.response.forks);
                assert.equal('', data.response.host_config_key);
                assert.equal(2, data.response.id);
                assert.equal(1, data.response.inventory);
                assert.equal(1, data.response.job_slice_count);
                assert.equal('', data.response.job_tags);
                assert.equal('run', data.response.job_type);
                assert.equal(false, data.response.last_job_failed);
                assert.equal('', data.response.limit);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('jt', data.response.name);
                assert.equal('helloworld.yml', data.response.playbook);
                assert.equal(1, data.response.project);
                assert.equal('object', typeof data.response.related);
                assert.equal('', data.response.scm_branch);
                assert.equal('', data.response.skip_tags);
                assert.equal('', data.response.start_at_task);
                assert.equal('never updated', data.response.status);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(false, data.response.survey_enabled);
                assert.equal(0, data.response.timeout);
                assert.equal('job_template', data.response.type);
                assert.equal('/api/v2/job_templates/2/', data.response.url);
                assert.equal(false, data.response.use_fact_cache);
                assert.equal(0, data.response.verbosity);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesAccessListList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesAccessListList(jobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesAccessListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesActivityStreamList(jobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesCallbackList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobTemplatesJobTemplatesCallbackList(jobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('foo', data.response.host_config_key);
                assert.equal(true, Array.isArray(data.response.matching_hosts));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesCallbackList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesCopyList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesCopyList(jobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesCopyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesCredentialsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobTemplatesJobTemplatesCredentialsList(jobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesCredentialsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesExtraCredentialsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobTemplatesJobTemplatesExtraCredentialsList(jobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(0, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesExtraCredentialsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesInstanceGroupsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobTemplatesJobTemplatesInstanceGroupsList(jobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesInstanceGroupsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesJobsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesJobsList(jobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesJobsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesLabelsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesLabelsList(jobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesLabelsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesLaunchRead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobTemplatesJobTemplatesLaunchRead(jobTemplatesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.ask_credential_on_launch);
                assert.equal(false, data.response.ask_diff_mode_on_launch);
                assert.equal(false, data.response.ask_inventory_on_launch);
                assert.equal(false, data.response.ask_job_type_on_launch);
                assert.equal(false, data.response.ask_limit_on_launch);
                assert.equal(false, data.response.ask_scm_branch_on_launch);
                assert.equal(false, data.response.ask_skip_tags_on_launch);
                assert.equal(false, data.response.ask_tags_on_launch);
                assert.equal(false, data.response.ask_variables_on_launch);
                assert.equal(false, data.response.ask_verbosity_on_launch);
                assert.equal(false, data.response.can_start_without_user_input);
                assert.equal(false, data.response.credential_needed_to_start);
                assert.equal('object', typeof data.response.defaults);
                assert.equal(false, data.response.inventory_needed_to_start);
                assert.equal('object', typeof data.response.job_template_data);
                assert.equal(true, Array.isArray(data.response.passwords_needed_to_start));
                assert.equal(false, data.response.survey_enabled);
                assert.equal(true, Array.isArray(data.response.variables_needed_to_start));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesLaunchRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesNotificationTemplatesErrorList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesNotificationTemplatesErrorList(jobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesNotificationTemplatesErrorList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesNotificationTemplatesStartedList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobTemplatesJobTemplatesNotificationTemplatesStartedList(jobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesNotificationTemplatesStartedList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesNotificationTemplatesSuccessList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesNotificationTemplatesSuccessList(jobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesNotificationTemplatesSuccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesObjectRolesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesObjectRolesList(jobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesObjectRolesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesSchedulesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesSchedulesList(jobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesSchedulesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesSliceWorkflowJobsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesSliceWorkflowJobsList(jobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesSliceWorkflowJobsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesSurveySpecList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesSurveySpecList(jobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesSurveySpecList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobsId = 'fakedata';
    describe('#jobsJobsCancelCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobsJobsCancelCreate(jobsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsCancelCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsCreateScheduleCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobsJobsCreateScheduleCreate(jobsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsCreateScheduleCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const jobsJobsJobsRelaunchCreateBodyParam = {};
    describe('#jobsJobsRelaunchCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobsJobsRelaunchCreate(jobsId, jobsJobsJobsRelaunchCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.allow_simultaneous);
                assert.equal('object', typeof data.response.artifacts);
                assert.equal('', data.response.controller_node);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(false, data.response.diff_mode);
                assert.equal(0, data.response.elapsed);
                assert.equal(false, data.response.event_processing_finished);
                assert.equal('', data.response.execution_node);
                assert.equal('{}', data.response.extra_vars);
                assert.equal(false, data.response.failed);
                assert.equal(false, data.response.force_handlers);
                assert.equal(0, data.response.forks);
                assert.equal(2, data.response.id);
                assert.equal(1, data.response.inventory);
                assert.equal(2, data.response.job);
                assert.equal('', data.response.job_args);
                assert.equal('', data.response.job_cwd);
                assert.equal('object', typeof data.response.job_env);
                assert.equal('', data.response.job_explanation);
                assert.equal(1, data.response.job_slice_count);
                assert.equal(0, data.response.job_slice_number);
                assert.equal('', data.response.job_tags);
                assert.equal(2, data.response.job_template);
                assert.equal('run', data.response.job_type);
                assert.equal('relaunch', data.response.launch_type);
                assert.equal('host3', data.response.limit);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('testjt', data.response.name);
                assert.equal(true, Array.isArray(data.response.passwords_needed_to_start));
                assert.equal('', data.response.playbook);
                assert.equal(1, data.response.project);
                assert.equal('object', typeof data.response.related);
                assert.equal('', data.response.result_traceback);
                assert.equal('', data.response.scm_branch);
                assert.equal('', data.response.scm_revision);
                assert.equal('', data.response.skip_tags);
                assert.equal('', data.response.start_at_task);
                assert.equal('pending', data.response.status);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(0, data.response.timeout);
                assert.equal('job', data.response.type);
                assert.equal(2, data.response.unified_job_template);
                assert.equal('/api/v2/jobs/2/', data.response.url);
                assert.equal(false, data.response.use_fact_cache);
                assert.equal(0, data.response.verbosity);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsRelaunchCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobsJobsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsRead0 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobsJobsRead0(jobsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.allow_simultaneous);
                assert.equal('object', typeof data.response.artifacts);
                assert.equal('', data.response.controller_node);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(false, data.response.diff_mode);
                assert.equal(0, data.response.elapsed);
                assert.equal(false, data.response.event_processing_finished);
                assert.equal('', data.response.execution_node);
                assert.equal('{}', data.response.extra_vars);
                assert.equal(false, data.response.failed);
                assert.equal(false, data.response.force_handlers);
                assert.equal(0, data.response.forks);
                assert.equal('object', typeof data.response.host_status_counts);
                assert.equal(1, data.response.id);
                assert.equal('', data.response.job_args);
                assert.equal('', data.response.job_cwd);
                assert.equal('object', typeof data.response.job_env);
                assert.equal('', data.response.job_explanation);
                assert.equal(1, data.response.job_slice_count);
                assert.equal(0, data.response.job_slice_number);
                assert.equal('', data.response.job_tags);
                assert.equal(1, data.response.job_template);
                assert.equal('run', data.response.job_type);
                assert.equal('manual', data.response.launch_type);
                assert.equal('', data.response.limit);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('', data.response.name);
                assert.equal(true, Array.isArray(data.response.passwords_needed_to_start));
                assert.equal('', data.response.playbook);
                assert.equal('object', typeof data.response.playbook_counts);
                assert.equal('object', typeof data.response.related);
                assert.equal('', data.response.result_traceback);
                assert.equal('', data.response.scm_branch);
                assert.equal('', data.response.scm_revision);
                assert.equal('', data.response.skip_tags);
                assert.equal('', data.response.start_at_task);
                assert.equal('new', data.response.status);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(0, data.response.timeout);
                assert.equal('job', data.response.type);
                assert.equal(1, data.response.unified_job_template);
                assert.equal('/api/v2/jobs/1/', data.response.url);
                assert.equal(false, data.response.use_fact_cache);
                assert.equal(0, data.response.verbosity);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsRead0', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobsJobsActivityStreamList(jobsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsCancelRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobsJobsCancelRead(jobsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsCancelRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsCreateScheduleRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobsJobsCreateScheduleRead(jobsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsCreateScheduleRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsCredentialsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobsJobsCredentialsList(jobsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsCredentialsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsExtraCredentialsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jobsJobsExtraCredentialsList(jobsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(0, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsExtraCredentialsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsJobEventsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobsJobsJobEventsList(jobsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsJobEventsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsJobHostSummariesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobsJobsJobHostSummariesList(jobsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsJobHostSummariesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsLabelsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobsJobsLabelsList(jobsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsLabelsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsNotificationsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobsJobsNotificationsList(jobsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsNotificationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsRelaunchRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobsJobsRelaunchRead(jobsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsRelaunchRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsStdoutRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobsJobsStdoutRead(jobsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsStdoutRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const labelsLabelsLabelsCreateBodyParam = {
      name: 'string',
      organization: 3
    };
    describe('#labelsLabelsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.labelsLabelsCreate(labelsLabelsLabelsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Labels', 'labelsLabelsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#labelsLabelsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.labelsLabelsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Labels', 'labelsLabelsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const labelsId = 'fakedata';
    const labelsLabelsLabelsUpdateBodyParam = {
      name: 'string',
      organization: 3
    };
    describe('#labelsLabelsUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.labelsLabelsUpdate(labelsId, null, labelsLabelsLabelsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Labels', 'labelsLabelsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const labelsLabelsLabelsPartialUpdateBodyParam = {
      name: 'string',
      organization: 9
    };
    describe('#labelsLabelsPartialUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.labelsLabelsPartialUpdate(labelsId, null, labelsLabelsLabelsPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Labels', 'labelsLabelsPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#labelsLabelsRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.labelsLabelsRead(labelsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Labels', 'labelsLabelsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let usersId = 'fakedata';
    const usersUsersUsersCreateBodyParam = {};
    describe('#usersUsersCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.usersUsersCreate(usersUsersUsersCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.auth));
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('a@a.com', data.response.email);
                assert.equal('a', data.response.first_name);
                assert.equal(3, data.response.id);
                assert.equal(false, data.response.is_superuser);
                assert.equal(false, data.response.is_system_auditor);
                assert.equal('a', data.response.last_name);
                assert.equal('', data.response.ldap_dn);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('user', data.response.type);
                assert.equal('/api/v2/users/3/', data.response.url);
                assert.equal('affable', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              usersId = data.response.id;
              saveMockData('Users', 'usersUsersCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUsersUsersApplicationsCreateBodyParam = {
      authorization_grant_type: 'string',
      client_type: 'string',
      name: 'string',
      organization: 3
    };
    describe('#usersUsersApplicationsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersApplicationsCreate(usersId, usersUsersUsersApplicationsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersApplicationsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUsersUsersAuthorizedTokensCreateBodyParam = {};
    describe('#usersUsersAuthorizedTokensCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.usersUsersAuthorizedTokensCreate(usersId, usersUsersUsersAuthorizedTokensCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.application);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.expires);
                assert.equal(3, data.response.id);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('CGNFqetTpUshseNpfxbNTNCQxf7bPj', data.response.refresh_token);
                assert.equal('object', typeof data.response.related);
                assert.equal('read', data.response.scope);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('1240AUFU6Xn5NSJgboHNcLKC9GYbDf', data.response.token);
                assert.equal('o_auth2_access_token', data.response.type);
                assert.equal('/api/v2/tokens/3/', data.response.url);
                assert.equal(1, data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersAuthorizedTokensCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUsersUsersCredentialsCreateBodyParam = {};
    describe('#usersUsersCredentialsCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.usersUsersCredentialsCreate(usersId, usersUsersUsersCredentialsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.cloud);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal(1, data.response.credential_type);
                assert.equal('', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal('object', typeof data.response.inputs);
                assert.equal('ssh', data.response.kind);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('Some name', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('credential', data.response.type);
                assert.equal('/api/v2/credentials/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersCredentialsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUsersUsersPersonalTokensCreateBodyParam = {
      description: 'string',
      scope: 'string'
    };
    describe('#usersUsersPersonalTokensCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersPersonalTokensCreate(usersId, usersUsersUsersPersonalTokensCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersPersonalTokensCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUsersUsersRolesCreateBodyParam = {};
    describe('#usersUsersRolesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersRolesCreate(usersId, usersUsersUsersRolesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersRolesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUsersUsersTokensCreateBodyParam = {};
    describe('#usersUsersTokensCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.usersUsersTokensCreate(usersId, usersUsersUsersTokensCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.expires);
                assert.equal(2, data.response.id);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('object', typeof data.response.related);
                assert.equal('read', data.response.scope);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('KnuQ7R5W8HLXrsDDfAF6KvgZCk0yug', data.response.token);
                assert.equal('o_auth2_access_token', data.response.type);
                assert.equal('/api/v2/tokens/2/', data.response.url);
                assert.equal(2, data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersTokensCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersMeList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersMeList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersMeList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUsersUsersUpdateBodyParam = {
      username: 'string'
    };
    describe('#usersUsersUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersUpdate(usersId, null, usersUsersUsersUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUsersUsersPartialUpdateBodyParam = {
      email: 'string',
      first_name: 'string',
      is_superuser: 'string',
      is_system_auditor: true,
      last_login: 'string',
      last_name: 'string',
      password: 'string',
      username: 'string'
    };
    describe('#usersUsersPartialUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersPartialUpdate(usersId, null, usersUsersUsersPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersRead(usersId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersAccessListList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersAccessListList(usersId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersAccessListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersActivityStreamList(usersId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersAdminOfOrganizationsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersAdminOfOrganizationsList(usersId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersAdminOfOrganizationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersApplicationsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersApplicationsList(usersId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersApplicationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersAuthorizedTokensList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersAuthorizedTokensList(usersId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersAuthorizedTokensList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersCredentialsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.usersUsersCredentialsList(usersId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersCredentialsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersOrganizationsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersOrganizationsList(usersId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersOrganizationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersPersonalTokensList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersPersonalTokensList(usersId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersPersonalTokensList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersProjectsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersProjectsList(usersId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersProjectsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersRolesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.usersUsersRolesList(usersId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersRolesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersTeamsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersTeamsList(usersId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersTeamsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersTokensList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.usersUsersTokensList(usersId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersTokensList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#metricsMetricsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.metricsMetricsList((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Metrics', 'metricsMetricsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const notificationTemplatesNotificationTemplatesNotificationTemplatesCreateBodyParam = {
      name: 'string',
      notification_type: 'string',
      organization: 4
    };
    describe('#notificationTemplatesNotificationTemplatesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesCreate(notificationTemplatesNotificationTemplatesNotificationTemplatesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationTemplates', 'notificationTemplatesNotificationTemplatesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const notificationTemplatesId = 'fakedata';
    const notificationTemplatesNotificationTemplatesNotificationTemplatesCopyCreateBodyParam = {
      name: 'string'
    };
    describe('#notificationTemplatesNotificationTemplatesCopyCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesCopyCreate(notificationTemplatesId, notificationTemplatesNotificationTemplatesNotificationTemplatesCopyCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationTemplates', 'notificationTemplatesNotificationTemplatesCopyCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationTemplatesNotificationTemplatesTestCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesTestCreate(notificationTemplatesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationTemplates', 'notificationTemplatesNotificationTemplatesTestCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationTemplatesNotificationTemplatesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationTemplates', 'notificationTemplatesNotificationTemplatesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const notificationTemplatesNotificationTemplatesNotificationTemplatesUpdateBodyParam = {
      name: 'string',
      notification_type: 'string',
      organization: 8
    };
    describe('#notificationTemplatesNotificationTemplatesUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesUpdate(notificationTemplatesId, null, notificationTemplatesNotificationTemplatesNotificationTemplatesUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationTemplates', 'notificationTemplatesNotificationTemplatesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const notificationTemplatesNotificationTemplatesNotificationTemplatesPartialUpdateBodyParam = {
      description: 'string',
      name: 'string',
      notification_configuration: 'string',
      notification_type: 'string',
      organization: 8
    };
    describe('#notificationTemplatesNotificationTemplatesPartialUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesPartialUpdate(notificationTemplatesId, null, notificationTemplatesNotificationTemplatesNotificationTemplatesPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationTemplates', 'notificationTemplatesNotificationTemplatesPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationTemplatesNotificationTemplatesRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesRead(notificationTemplatesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationTemplates', 'notificationTemplatesNotificationTemplatesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationTemplatesNotificationTemplatesCopyList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesCopyList(notificationTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationTemplates', 'notificationTemplatesNotificationTemplatesCopyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationTemplatesNotificationTemplatesNotificationsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesNotificationsList(notificationTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationTemplates', 'notificationTemplatesNotificationTemplatesNotificationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationsNotificationsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.notificationsNotificationsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Notifications', 'notificationsNotificationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const notificationsId = 'fakedata';
    describe('#notificationsNotificationsRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.notificationsNotificationsRead(notificationsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Notifications', 'notificationsNotificationsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let organizationsId = 'fakedata';
    const organizationsOrganizationsOrganizationsCreateBodyParam = {};
    describe('#organizationsOrganizationsCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.organizationsOrganizationsCreate(organizationsOrganizationsOrganizationsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('my description', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal(0, data.response.max_hosts);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('new org', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('organization', data.response.type);
                assert.equal('/api/v2/organizations/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              organizationsId = data.response.id;
              saveMockData('Organizations', 'organizationsOrganizationsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationsOrganizationsOrganizationsAdminsCreateBodyParam = {};
    describe('#organizationsOrganizationsAdminsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsAdminsCreate(organizationsId, organizationsOrganizationsOrganizationsAdminsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsAdminsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationsOrganizationsOrganizationsApplicationsCreateBodyParam = {
      authorization_grant_type: 'string',
      client_type: 'string',
      name: 'string',
      organization: 2
    };
    describe('#organizationsOrganizationsApplicationsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsApplicationsCreate(organizationsId, organizationsOrganizationsOrganizationsApplicationsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsApplicationsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationsOrganizationsOrganizationsCredentialsCreateBodyParam = {
      credential_type: 1,
      name: 'string'
    };
    describe('#organizationsOrganizationsCredentialsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsCredentialsCreate(organizationsId, organizationsOrganizationsOrganizationsCredentialsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsCredentialsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationsOrganizationsOrganizationsInstanceGroupsCreateBodyParam = {};
    describe('#organizationsOrganizationsInstanceGroupsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsInstanceGroupsCreate(organizationsId, organizationsOrganizationsOrganizationsInstanceGroupsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsInstanceGroupsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationsOrganizationsOrganizationsNotificationTemplatesCreateBodyParam = {
      name: 'string',
      notification_type: 'string',
      organization: 6
    };
    describe('#organizationsOrganizationsNotificationTemplatesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsNotificationTemplatesCreate(organizationsId, organizationsOrganizationsOrganizationsNotificationTemplatesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsNotificationTemplatesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationsOrganizationsOrganizationsNotificationTemplatesErrorCreateBodyParam = {
      name: 'string',
      notification_type: 'string',
      organization: 6
    };
    describe('#organizationsOrganizationsNotificationTemplatesErrorCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsNotificationTemplatesErrorCreate(organizationsId, organizationsOrganizationsOrganizationsNotificationTemplatesErrorCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsNotificationTemplatesErrorCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationsOrganizationsOrganizationsNotificationTemplatesStartedCreateBodyParam = {};
    describe('#organizationsOrganizationsNotificationTemplatesStartedCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsNotificationTemplatesStartedCreate(organizationsId, organizationsOrganizationsOrganizationsNotificationTemplatesStartedCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsNotificationTemplatesStartedCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationsOrganizationsOrganizationsNotificationTemplatesSuccessCreateBodyParam = {
      name: 'string',
      notification_type: 'string',
      organization: 7
    };
    describe('#organizationsOrganizationsNotificationTemplatesSuccessCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsNotificationTemplatesSuccessCreate(organizationsId, organizationsOrganizationsOrganizationsNotificationTemplatesSuccessCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsNotificationTemplatesSuccessCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationsOrganizationsOrganizationsProjectsCreateBodyParam = {
      name: 'string'
    };
    describe('#organizationsOrganizationsProjectsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsProjectsCreate(organizationsId, organizationsOrganizationsOrganizationsProjectsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsProjectsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationsOrganizationsOrganizationsTeamsCreateBodyParam = {
      name: 'string',
      organization: 9
    };
    describe('#organizationsOrganizationsTeamsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsTeamsCreate(organizationsId, organizationsOrganizationsOrganizationsTeamsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsTeamsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationsOrganizationsOrganizationsUsersCreateBodyParam = {};
    describe('#organizationsOrganizationsUsersCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsUsersCreate(organizationsId, organizationsOrganizationsOrganizationsUsersCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsUsersCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationsOrganizationsOrganizationsWorkflowJobTemplatesCreateBodyParam = {
      name: 'string'
    };
    describe('#organizationsOrganizationsWorkflowJobTemplatesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsWorkflowJobTemplatesCreate(organizationsId, organizationsOrganizationsOrganizationsWorkflowJobTemplatesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsWorkflowJobTemplatesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.organizationsOrganizationsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationsOrganizationsOrganizationsUpdateBodyParam = {};
    describe('#organizationsOrganizationsUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.organizationsOrganizationsUpdate(organizationsId, null, organizationsOrganizationsOrganizationsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const organizationsOrganizationsOrganizationsPartialUpdateBodyParam = {};
    describe('#organizationsOrganizationsPartialUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.organizationsOrganizationsPartialUpdate(organizationsId, null, organizationsOrganizationsOrganizationsPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsRead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.organizationsOrganizationsRead(organizationsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('/venv/tmpmxvz6gwz/', data.response.custom_virtualenv);
                assert.equal('test-org-desc', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal(0, data.response.max_hosts);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('test-org', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('organization', data.response.type);
                assert.equal('/api/v2/organizations/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsAccessListList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsAccessListList(organizationsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsAccessListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsActivityStreamList(organizationsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsAdminsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.organizationsOrganizationsAdminsList(organizationsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsAdminsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsApplicationsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsApplicationsList(organizationsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsApplicationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsCredentialsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.organizationsOrganizationsCredentialsList(organizationsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(0, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsCredentialsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsInstanceGroupsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.organizationsOrganizationsInstanceGroupsList(organizationsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(5, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsInstanceGroupsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsInventoriesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.organizationsOrganizationsInventoriesList(organizationsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsInventoriesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsNotificationTemplatesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsNotificationTemplatesList(organizationsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsNotificationTemplatesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsNotificationTemplatesErrorList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsNotificationTemplatesErrorList(organizationsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsNotificationTemplatesErrorList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsNotificationTemplatesStartedList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.organizationsOrganizationsNotificationTemplatesStartedList(organizationsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsNotificationTemplatesStartedList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsNotificationTemplatesSuccessList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsNotificationTemplatesSuccessList(organizationsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsNotificationTemplatesSuccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsObjectRolesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsObjectRolesList(organizationsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsObjectRolesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsProjectsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.organizationsOrganizationsProjectsList(organizationsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsProjectsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsTeamsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsTeamsList(organizationsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsTeamsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsUsersList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.organizationsOrganizationsUsersList(organizationsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsUsersList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsWorkflowJobTemplatesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsWorkflowJobTemplatesList(organizationsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsWorkflowJobTemplatesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectUpdatesId = 'fakedata';
    describe('#projectUpdatesProjectUpdatesCancelCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectUpdatesProjectUpdatesCancelCreate(projectUpdatesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProjectUpdates', 'projectUpdatesProjectUpdatesCancelCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectUpdatesProjectUpdatesList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProjectUpdates', 'projectUpdatesProjectUpdatesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesRead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.projectUpdatesProjectUpdatesRead(projectUpdatesId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(0, data.response.elapsed);
                assert.equal(false, data.response.event_processing_finished);
                assert.equal('', data.response.execution_node);
                assert.equal(false, data.response.failed);
                assert.equal('object', typeof data.response.host_status_counts);
                assert.equal(2, data.response.id);
                assert.equal('', data.response.job_args);
                assert.equal('', data.response.job_cwd);
                assert.equal('object', typeof data.response.job_env);
                assert.equal('', data.response.job_explanation);
                assert.equal('check', data.response.job_type);
                assert.equal('manual', data.response.launch_type);
                assert.equal('', data.response.local_path);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('', data.response.name);
                assert.equal('object', typeof data.response.playbook_counts);
                assert.equal(2, data.response.project);
                assert.equal('object', typeof data.response.related);
                assert.equal('', data.response.result_traceback);
                assert.equal('', data.response.scm_branch);
                assert.equal(false, data.response.scm_clean);
                assert.equal(false, data.response.scm_delete_on_update);
                assert.equal('', data.response.scm_refspec);
                assert.equal('', data.response.scm_revision);
                assert.equal('', data.response.scm_type);
                assert.equal('', data.response.scm_url);
                assert.equal('new', data.response.status);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(0, data.response.timeout);
                assert.equal('project_update', data.response.type);
                assert.equal(2, data.response.unified_job_template);
                assert.equal('/api/v2/project_updates/2/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProjectUpdates', 'projectUpdatesProjectUpdatesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesCancelRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectUpdatesProjectUpdatesCancelRead(projectUpdatesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProjectUpdates', 'projectUpdatesProjectUpdatesCancelRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesEventsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectUpdatesProjectUpdatesEventsList(projectUpdatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProjectUpdates', 'projectUpdatesProjectUpdatesEventsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesNotificationsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectUpdatesProjectUpdatesNotificationsList(projectUpdatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProjectUpdates', 'projectUpdatesProjectUpdatesNotificationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesScmInventoryUpdatesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectUpdatesProjectUpdatesScmInventoryUpdatesList(projectUpdatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProjectUpdates', 'projectUpdatesProjectUpdatesScmInventoryUpdatesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesStdoutRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectUpdatesProjectUpdatesStdoutRead(projectUpdatesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProjectUpdates', 'projectUpdatesProjectUpdatesStdoutRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let projectsId = 'fakedata';
    const projectsProjectsProjectsCreateBodyParam = {};
    describe('#projectsProjectsCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.projectsProjectsCreate(projectsProjectsProjectsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, data.response.allow_override);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal(false, data.response.last_job_failed);
                assert.equal(false, data.response.last_update_failed);
                assert.equal('', data.response.local_path);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('fooo', data.response.name);
                assert.equal(1, data.response.organization);
                assert.equal('object', typeof data.response.related);
                assert.equal('', data.response.scm_branch);
                assert.equal(false, data.response.scm_clean);
                assert.equal(false, data.response.scm_delete_on_update);
                assert.equal('', data.response.scm_refspec);
                assert.equal('', data.response.scm_revision);
                assert.equal('', data.response.scm_type);
                assert.equal(0, data.response.scm_update_cache_timeout);
                assert.equal(false, data.response.scm_update_on_launch);
                assert.equal('', data.response.scm_url);
                assert.equal('missing', data.response.status);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(0, data.response.timeout);
                assert.equal('project', data.response.type);
                assert.equal('/api/v2/projects/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              projectsId = data.response.id;
              saveMockData('Projects', 'projectsProjectsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectsProjectsProjectsCopyCreateBodyParam = {
      name: 'string'
    };
    describe('#projectsProjectsCopyCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsCopyCreate(projectsId, projectsProjectsProjectsCopyCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsCopyCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectsProjectsProjectsNotificationTemplatesErrorCreateBodyParam = {
      name: 'string',
      notification_type: 'string',
      organization: 3
    };
    describe('#projectsProjectsNotificationTemplatesErrorCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsNotificationTemplatesErrorCreate(projectsId, projectsProjectsProjectsNotificationTemplatesErrorCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsNotificationTemplatesErrorCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectsProjectsProjectsNotificationTemplatesStartedCreateBodyParam = {};
    describe('#projectsProjectsNotificationTemplatesStartedCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsNotificationTemplatesStartedCreate(projectsId, projectsProjectsProjectsNotificationTemplatesStartedCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsNotificationTemplatesStartedCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectsProjectsProjectsNotificationTemplatesSuccessCreateBodyParam = {
      name: 'string',
      notification_type: 'string',
      organization: 1
    };
    describe('#projectsProjectsNotificationTemplatesSuccessCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsNotificationTemplatesSuccessCreate(projectsId, projectsProjectsProjectsNotificationTemplatesSuccessCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsNotificationTemplatesSuccessCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectsProjectsProjectsSchedulesCreateBodyParam = {
      name: 'string',
      rrule: 'string',
      unified_job_template: 8
    };
    describe('#projectsProjectsSchedulesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsSchedulesCreate(projectsId, projectsProjectsProjectsSchedulesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsSchedulesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsUpdateCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsUpdateCreate(projectsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsUpdateCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectsProjectsProjectsUpdate0BodyParam = {
      name: 'string'
    };
    describe('#projectsProjectsUpdate0 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsUpdate0(projectsId, null, projectsProjectsProjectsUpdate0BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsUpdate0', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const projectsProjectsProjectsPartialUpdateBodyParam = {};
    describe('#projectsProjectsPartialUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.projectsProjectsPartialUpdate(projectsId, null, projectsProjectsProjectsPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsRead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.projectsProjectsRead(projectsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.allow_override);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('manual-proj-desc', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal(false, data.response.last_job_failed);
                assert.equal(false, data.response.last_update_failed);
                assert.equal('_92__test_proj', data.response.local_path);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('test-manual-proj', data.response.name);
                assert.equal(1, data.response.organization);
                assert.equal('object', typeof data.response.related);
                assert.equal('', data.response.scm_branch);
                assert.equal(false, data.response.scm_clean);
                assert.equal(false, data.response.scm_delete_on_update);
                assert.equal('', data.response.scm_refspec);
                assert.equal('', data.response.scm_revision);
                assert.equal('', data.response.scm_type);
                assert.equal(0, data.response.scm_update_cache_timeout);
                assert.equal(false, data.response.scm_update_on_launch);
                assert.equal('', data.response.scm_url);
                assert.equal('missing', data.response.status);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal(0, data.response.timeout);
                assert.equal('project', data.response.type);
                assert.equal('/api/v2/projects/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsAccessListList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.projectsProjectsAccessListList(projectsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(4, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsAccessListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsActivityStreamList(projectsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsCopyList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsCopyList(projectsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsCopyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsInventoriesRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsInventoriesRead(projectsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsInventoriesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsNotificationTemplatesErrorList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsNotificationTemplatesErrorList(projectsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsNotificationTemplatesErrorList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsNotificationTemplatesStartedList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.projectsProjectsNotificationTemplatesStartedList(projectsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsNotificationTemplatesStartedList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsNotificationTemplatesSuccessList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsNotificationTemplatesSuccessList(projectsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsNotificationTemplatesSuccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsObjectRolesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsObjectRolesList(projectsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsObjectRolesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsPlaybooksRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsPlaybooksRead(projectsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsPlaybooksRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsProjectUpdatesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsProjectUpdatesList(projectsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsProjectUpdatesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsSchedulesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsSchedulesList(projectsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsSchedulesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsScmInventorySourcesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsScmInventorySourcesList(projectsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsScmInventorySourcesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsTeamsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsTeamsList(projectsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsTeamsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsUpdateRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsUpdateRead(projectsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsUpdateRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rolesId = 'fakedata';
    const rolesRolesRolesTeamsCreateBodyParam = {};
    describe('#rolesRolesTeamsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rolesRolesTeamsCreate(rolesId, rolesRolesRolesTeamsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'rolesRolesTeamsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rolesRolesRolesUsersCreateBodyParam = {};
    describe('#rolesRolesUsersCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rolesRolesUsersCreate(rolesId, rolesRolesRolesUsersCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'rolesRolesUsersCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesRolesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rolesRolesList(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(2, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'rolesRolesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesRolesRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rolesRolesRead(rolesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'rolesRolesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesRolesChildrenList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rolesRolesChildrenList(rolesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'rolesRolesChildrenList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesRolesParentsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rolesRolesParentsList(rolesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'rolesRolesParentsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesRolesTeamsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rolesRolesTeamsList(rolesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'rolesRolesTeamsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rolesRolesUsersList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.rolesRolesUsersList(rolesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Roles', 'rolesRolesUsersList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulesSchedulesSchedulesCreateBodyParam = {
      name: 'string',
      rrule: 'string',
      unified_job_template: 2
    };
    describe('#schedulesSchedulesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.schedulesSchedulesCreate(schedulesSchedulesSchedulesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'schedulesSchedulesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulesSchedulesSchedulesPreviewCreateBodyParam = {};
    describe('#schedulesSchedulesPreviewCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.schedulesSchedulesPreviewCreate(schedulesSchedulesSchedulesPreviewCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.local));
                assert.equal(true, Array.isArray(data.response.utc));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'schedulesSchedulesPreviewCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulesId = 'fakedata';
    const schedulesSchedulesSchedulesCredentialsCreateBodyParam = {
      credential_type: 3,
      name: 'string'
    };
    describe('#schedulesSchedulesCredentialsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.schedulesSchedulesCredentialsCreate(schedulesId, schedulesSchedulesSchedulesCredentialsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'schedulesSchedulesCredentialsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.schedulesSchedulesList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'schedulesSchedulesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesZoneinfoList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.schedulesSchedulesZoneinfoList((data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
                assert.equal('object', typeof data.response[4]);
                assert.equal('object', typeof data.response[5]);
                assert.equal('object', typeof data.response[6]);
                assert.equal('object', typeof data.response[7]);
                assert.equal('object', typeof data.response[8]);
                assert.equal('object', typeof data.response[9]);
                assert.equal('object', typeof data.response[10]);
                assert.equal('object', typeof data.response[11]);
                assert.equal('object', typeof data.response[12]);
                assert.equal('object', typeof data.response[13]);
                assert.equal('object', typeof data.response[14]);
                assert.equal('object', typeof data.response[15]);
                assert.equal('object', typeof data.response[16]);
                assert.equal('object', typeof data.response[17]);
                assert.equal('object', typeof data.response[18]);
                assert.equal('object', typeof data.response[19]);
                assert.equal('object', typeof data.response[20]);
                assert.equal('object', typeof data.response[21]);
                assert.equal('object', typeof data.response[22]);
                assert.equal('object', typeof data.response[23]);
                assert.equal('object', typeof data.response[24]);
                assert.equal('object', typeof data.response[25]);
                assert.equal('object', typeof data.response[26]);
                assert.equal('object', typeof data.response[27]);
                assert.equal('object', typeof data.response[28]);
                assert.equal('object', typeof data.response[29]);
                assert.equal('object', typeof data.response[30]);
                assert.equal('object', typeof data.response[31]);
                assert.equal('object', typeof data.response[32]);
                assert.equal('object', typeof data.response[33]);
                assert.equal('object', typeof data.response[34]);
                assert.equal('object', typeof data.response[35]);
                assert.equal('object', typeof data.response[36]);
                assert.equal('object', typeof data.response[37]);
                assert.equal('object', typeof data.response[38]);
                assert.equal('object', typeof data.response[39]);
                assert.equal('object', typeof data.response[40]);
                assert.equal('object', typeof data.response[41]);
                assert.equal('object', typeof data.response[42]);
                assert.equal('object', typeof data.response[43]);
                assert.equal('object', typeof data.response[44]);
                assert.equal('object', typeof data.response[45]);
                assert.equal('object', typeof data.response[46]);
                assert.equal('object', typeof data.response[47]);
                assert.equal('object', typeof data.response[48]);
                assert.equal('object', typeof data.response[49]);
                assert.equal('object', typeof data.response[50]);
                assert.equal('object', typeof data.response[51]);
                assert.equal('object', typeof data.response[52]);
                assert.equal('object', typeof data.response[53]);
                assert.equal('object', typeof data.response[54]);
                assert.equal('object', typeof data.response[55]);
                assert.equal('object', typeof data.response[56]);
                assert.equal('object', typeof data.response[57]);
                assert.equal('object', typeof data.response[58]);
                assert.equal('object', typeof data.response[59]);
                assert.equal('object', typeof data.response[60]);
                assert.equal('object', typeof data.response[61]);
                assert.equal('object', typeof data.response[62]);
                assert.equal('object', typeof data.response[63]);
                assert.equal('object', typeof data.response[64]);
                assert.equal('object', typeof data.response[65]);
                assert.equal('object', typeof data.response[66]);
                assert.equal('object', typeof data.response[67]);
                assert.equal('object', typeof data.response[68]);
                assert.equal('object', typeof data.response[69]);
                assert.equal('object', typeof data.response[70]);
                assert.equal('object', typeof data.response[71]);
                assert.equal('object', typeof data.response[72]);
                assert.equal('object', typeof data.response[73]);
                assert.equal('object', typeof data.response[74]);
                assert.equal('object', typeof data.response[75]);
                assert.equal('object', typeof data.response[76]);
                assert.equal('object', typeof data.response[77]);
                assert.equal('object', typeof data.response[78]);
                assert.equal('object', typeof data.response[79]);
                assert.equal('object', typeof data.response[80]);
                assert.equal('object', typeof data.response[81]);
                assert.equal('object', typeof data.response[82]);
                assert.equal('object', typeof data.response[83]);
                assert.equal('object', typeof data.response[84]);
                assert.equal('object', typeof data.response[85]);
                assert.equal('object', typeof data.response[86]);
                assert.equal('object', typeof data.response[87]);
                assert.equal('object', typeof data.response[88]);
                assert.equal('object', typeof data.response[89]);
                assert.equal('object', typeof data.response[90]);
                assert.equal('object', typeof data.response[91]);
                assert.equal('object', typeof data.response[92]);
                assert.equal('object', typeof data.response[93]);
                assert.equal('object', typeof data.response[94]);
                assert.equal('object', typeof data.response[95]);
                assert.equal('object', typeof data.response[96]);
                assert.equal('object', typeof data.response[97]);
                assert.equal('object', typeof data.response[98]);
                assert.equal('object', typeof data.response[99]);
                assert.equal('object', typeof data.response[100]);
                assert.equal('object', typeof data.response[101]);
                assert.equal('object', typeof data.response[102]);
                assert.equal('object', typeof data.response[103]);
                assert.equal('object', typeof data.response[104]);
                assert.equal('object', typeof data.response[105]);
                assert.equal('object', typeof data.response[106]);
                assert.equal('object', typeof data.response[107]);
                assert.equal('object', typeof data.response[108]);
                assert.equal('object', typeof data.response[109]);
                assert.equal('object', typeof data.response[110]);
                assert.equal('object', typeof data.response[111]);
                assert.equal('object', typeof data.response[112]);
                assert.equal('object', typeof data.response[113]);
                assert.equal('object', typeof data.response[114]);
                assert.equal('object', typeof data.response[115]);
                assert.equal('object', typeof data.response[116]);
                assert.equal('object', typeof data.response[117]);
                assert.equal('object', typeof data.response[118]);
                assert.equal('object', typeof data.response[119]);
                assert.equal('object', typeof data.response[120]);
                assert.equal('object', typeof data.response[121]);
                assert.equal('object', typeof data.response[122]);
                assert.equal('object', typeof data.response[123]);
                assert.equal('object', typeof data.response[124]);
                assert.equal('object', typeof data.response[125]);
                assert.equal('object', typeof data.response[126]);
                assert.equal('object', typeof data.response[127]);
                assert.equal('object', typeof data.response[128]);
                assert.equal('object', typeof data.response[129]);
                assert.equal('object', typeof data.response[130]);
                assert.equal('object', typeof data.response[131]);
                assert.equal('object', typeof data.response[132]);
                assert.equal('object', typeof data.response[133]);
                assert.equal('object', typeof data.response[134]);
                assert.equal('object', typeof data.response[135]);
                assert.equal('object', typeof data.response[136]);
                assert.equal('object', typeof data.response[137]);
                assert.equal('object', typeof data.response[138]);
                assert.equal('object', typeof data.response[139]);
                assert.equal('object', typeof data.response[140]);
                assert.equal('object', typeof data.response[141]);
                assert.equal('object', typeof data.response[142]);
                assert.equal('object', typeof data.response[143]);
                assert.equal('object', typeof data.response[144]);
                assert.equal('object', typeof data.response[145]);
                assert.equal('object', typeof data.response[146]);
                assert.equal('object', typeof data.response[147]);
                assert.equal('object', typeof data.response[148]);
                assert.equal('object', typeof data.response[149]);
                assert.equal('object', typeof data.response[150]);
                assert.equal('object', typeof data.response[151]);
                assert.equal('object', typeof data.response[152]);
                assert.equal('object', typeof data.response[153]);
                assert.equal('object', typeof data.response[154]);
                assert.equal('object', typeof data.response[155]);
                assert.equal('object', typeof data.response[156]);
                assert.equal('object', typeof data.response[157]);
                assert.equal('object', typeof data.response[158]);
                assert.equal('object', typeof data.response[159]);
                assert.equal('object', typeof data.response[160]);
                assert.equal('object', typeof data.response[161]);
                assert.equal('object', typeof data.response[162]);
                assert.equal('object', typeof data.response[163]);
                assert.equal('object', typeof data.response[164]);
                assert.equal('object', typeof data.response[165]);
                assert.equal('object', typeof data.response[166]);
                assert.equal('object', typeof data.response[167]);
                assert.equal('object', typeof data.response[168]);
                assert.equal('object', typeof data.response[169]);
                assert.equal('object', typeof data.response[170]);
                assert.equal('object', typeof data.response[171]);
                assert.equal('object', typeof data.response[172]);
                assert.equal('object', typeof data.response[173]);
                assert.equal('object', typeof data.response[174]);
                assert.equal('object', typeof data.response[175]);
                assert.equal('object', typeof data.response[176]);
                assert.equal('object', typeof data.response[177]);
                assert.equal('object', typeof data.response[178]);
                assert.equal('object', typeof data.response[179]);
                assert.equal('object', typeof data.response[180]);
                assert.equal('object', typeof data.response[181]);
                assert.equal('object', typeof data.response[182]);
                assert.equal('object', typeof data.response[183]);
                assert.equal('object', typeof data.response[184]);
                assert.equal('object', typeof data.response[185]);
                assert.equal('object', typeof data.response[186]);
                assert.equal('object', typeof data.response[187]);
                assert.equal('object', typeof data.response[188]);
                assert.equal('object', typeof data.response[189]);
                assert.equal('object', typeof data.response[190]);
                assert.equal('object', typeof data.response[191]);
                assert.equal('object', typeof data.response[192]);
                assert.equal('object', typeof data.response[193]);
                assert.equal('object', typeof data.response[194]);
                assert.equal('object', typeof data.response[195]);
                assert.equal('object', typeof data.response[196]);
                assert.equal('object', typeof data.response[197]);
                assert.equal('object', typeof data.response[198]);
                assert.equal('object', typeof data.response[199]);
                assert.equal('object', typeof data.response[200]);
                assert.equal('object', typeof data.response[201]);
                assert.equal('object', typeof data.response[202]);
                assert.equal('object', typeof data.response[203]);
                assert.equal('object', typeof data.response[204]);
                assert.equal('object', typeof data.response[205]);
                assert.equal('object', typeof data.response[206]);
                assert.equal('object', typeof data.response[207]);
                assert.equal('object', typeof data.response[208]);
                assert.equal('object', typeof data.response[209]);
                assert.equal('object', typeof data.response[210]);
                assert.equal('object', typeof data.response[211]);
                assert.equal('object', typeof data.response[212]);
                assert.equal('object', typeof data.response[213]);
                assert.equal('object', typeof data.response[214]);
                assert.equal('object', typeof data.response[215]);
                assert.equal('object', typeof data.response[216]);
                assert.equal('object', typeof data.response[217]);
                assert.equal('object', typeof data.response[218]);
                assert.equal('object', typeof data.response[219]);
                assert.equal('object', typeof data.response[220]);
                assert.equal('object', typeof data.response[221]);
                assert.equal('object', typeof data.response[222]);
                assert.equal('object', typeof data.response[223]);
                assert.equal('object', typeof data.response[224]);
                assert.equal('object', typeof data.response[225]);
                assert.equal('object', typeof data.response[226]);
                assert.equal('object', typeof data.response[227]);
                assert.equal('object', typeof data.response[228]);
                assert.equal('object', typeof data.response[229]);
                assert.equal('object', typeof data.response[230]);
                assert.equal('object', typeof data.response[231]);
                assert.equal('object', typeof data.response[232]);
                assert.equal('object', typeof data.response[233]);
                assert.equal('object', typeof data.response[234]);
                assert.equal('object', typeof data.response[235]);
                assert.equal('object', typeof data.response[236]);
                assert.equal('object', typeof data.response[237]);
                assert.equal('object', typeof data.response[238]);
                assert.equal('object', typeof data.response[239]);
                assert.equal('object', typeof data.response[240]);
                assert.equal('object', typeof data.response[241]);
                assert.equal('object', typeof data.response[242]);
                assert.equal('object', typeof data.response[243]);
                assert.equal('object', typeof data.response[244]);
                assert.equal('object', typeof data.response[245]);
                assert.equal('object', typeof data.response[246]);
                assert.equal('object', typeof data.response[247]);
                assert.equal('object', typeof data.response[248]);
                assert.equal('object', typeof data.response[249]);
                assert.equal('object', typeof data.response[250]);
                assert.equal('object', typeof data.response[251]);
                assert.equal('object', typeof data.response[252]);
                assert.equal('object', typeof data.response[253]);
                assert.equal('object', typeof data.response[254]);
                assert.equal('object', typeof data.response[255]);
                assert.equal('object', typeof data.response[256]);
                assert.equal('object', typeof data.response[257]);
                assert.equal('object', typeof data.response[258]);
                assert.equal('object', typeof data.response[259]);
                assert.equal('object', typeof data.response[260]);
                assert.equal('object', typeof data.response[261]);
                assert.equal('object', typeof data.response[262]);
                assert.equal('object', typeof data.response[263]);
                assert.equal('object', typeof data.response[264]);
                assert.equal('object', typeof data.response[265]);
                assert.equal('object', typeof data.response[266]);
                assert.equal('object', typeof data.response[267]);
                assert.equal('object', typeof data.response[268]);
                assert.equal('object', typeof data.response[269]);
                assert.equal('object', typeof data.response[270]);
                assert.equal('object', typeof data.response[271]);
                assert.equal('object', typeof data.response[272]);
                assert.equal('object', typeof data.response[273]);
                assert.equal('object', typeof data.response[274]);
                assert.equal('object', typeof data.response[275]);
                assert.equal('object', typeof data.response[276]);
                assert.equal('object', typeof data.response[277]);
                assert.equal('object', typeof data.response[278]);
                assert.equal('object', typeof data.response[279]);
                assert.equal('object', typeof data.response[280]);
                assert.equal('object', typeof data.response[281]);
                assert.equal('object', typeof data.response[282]);
                assert.equal('object', typeof data.response[283]);
                assert.equal('object', typeof data.response[284]);
                assert.equal('object', typeof data.response[285]);
                assert.equal('object', typeof data.response[286]);
                assert.equal('object', typeof data.response[287]);
                assert.equal('object', typeof data.response[288]);
                assert.equal('object', typeof data.response[289]);
                assert.equal('object', typeof data.response[290]);
                assert.equal('object', typeof data.response[291]);
                assert.equal('object', typeof data.response[292]);
                assert.equal('object', typeof data.response[293]);
                assert.equal('object', typeof data.response[294]);
                assert.equal('object', typeof data.response[295]);
                assert.equal('object', typeof data.response[296]);
                assert.equal('object', typeof data.response[297]);
                assert.equal('object', typeof data.response[298]);
                assert.equal('object', typeof data.response[299]);
                assert.equal('object', typeof data.response[300]);
                assert.equal('object', typeof data.response[301]);
                assert.equal('object', typeof data.response[302]);
                assert.equal('object', typeof data.response[303]);
                assert.equal('object', typeof data.response[304]);
                assert.equal('object', typeof data.response[305]);
                assert.equal('object', typeof data.response[306]);
                assert.equal('object', typeof data.response[307]);
                assert.equal('object', typeof data.response[308]);
                assert.equal('object', typeof data.response[309]);
                assert.equal('object', typeof data.response[310]);
                assert.equal('object', typeof data.response[311]);
                assert.equal('object', typeof data.response[312]);
                assert.equal('object', typeof data.response[313]);
                assert.equal('object', typeof data.response[314]);
                assert.equal('object', typeof data.response[315]);
                assert.equal('object', typeof data.response[316]);
                assert.equal('object', typeof data.response[317]);
                assert.equal('object', typeof data.response[318]);
                assert.equal('object', typeof data.response[319]);
                assert.equal('object', typeof data.response[320]);
                assert.equal('object', typeof data.response[321]);
                assert.equal('object', typeof data.response[322]);
                assert.equal('object', typeof data.response[323]);
                assert.equal('object', typeof data.response[324]);
                assert.equal('object', typeof data.response[325]);
                assert.equal('object', typeof data.response[326]);
                assert.equal('object', typeof data.response[327]);
                assert.equal('object', typeof data.response[328]);
                assert.equal('object', typeof data.response[329]);
                assert.equal('object', typeof data.response[330]);
                assert.equal('object', typeof data.response[331]);
                assert.equal('object', typeof data.response[332]);
                assert.equal('object', typeof data.response[333]);
                assert.equal('object', typeof data.response[334]);
                assert.equal('object', typeof data.response[335]);
                assert.equal('object', typeof data.response[336]);
                assert.equal('object', typeof data.response[337]);
                assert.equal('object', typeof data.response[338]);
                assert.equal('object', typeof data.response[339]);
                assert.equal('object', typeof data.response[340]);
                assert.equal('object', typeof data.response[341]);
                assert.equal('object', typeof data.response[342]);
                assert.equal('object', typeof data.response[343]);
                assert.equal('object', typeof data.response[344]);
                assert.equal('object', typeof data.response[345]);
                assert.equal('object', typeof data.response[346]);
                assert.equal('object', typeof data.response[347]);
                assert.equal('object', typeof data.response[348]);
                assert.equal('object', typeof data.response[349]);
                assert.equal('object', typeof data.response[350]);
                assert.equal('object', typeof data.response[351]);
                assert.equal('object', typeof data.response[352]);
                assert.equal('object', typeof data.response[353]);
                assert.equal('object', typeof data.response[354]);
                assert.equal('object', typeof data.response[355]);
                assert.equal('object', typeof data.response[356]);
                assert.equal('object', typeof data.response[357]);
                assert.equal('object', typeof data.response[358]);
                assert.equal('object', typeof data.response[359]);
                assert.equal('object', typeof data.response[360]);
                assert.equal('object', typeof data.response[361]);
                assert.equal('object', typeof data.response[362]);
                assert.equal('object', typeof data.response[363]);
                assert.equal('object', typeof data.response[364]);
                assert.equal('object', typeof data.response[365]);
                assert.equal('object', typeof data.response[366]);
                assert.equal('object', typeof data.response[367]);
                assert.equal('object', typeof data.response[368]);
                assert.equal('object', typeof data.response[369]);
                assert.equal('object', typeof data.response[370]);
                assert.equal('object', typeof data.response[371]);
                assert.equal('object', typeof data.response[372]);
                assert.equal('object', typeof data.response[373]);
                assert.equal('object', typeof data.response[374]);
                assert.equal('object', typeof data.response[375]);
                assert.equal('object', typeof data.response[376]);
                assert.equal('object', typeof data.response[377]);
                assert.equal('object', typeof data.response[378]);
                assert.equal('object', typeof data.response[379]);
                assert.equal('object', typeof data.response[380]);
                assert.equal('object', typeof data.response[381]);
                assert.equal('object', typeof data.response[382]);
                assert.equal('object', typeof data.response[383]);
                assert.equal('object', typeof data.response[384]);
                assert.equal('object', typeof data.response[385]);
                assert.equal('object', typeof data.response[386]);
                assert.equal('object', typeof data.response[387]);
                assert.equal('object', typeof data.response[388]);
                assert.equal('object', typeof data.response[389]);
                assert.equal('object', typeof data.response[390]);
                assert.equal('object', typeof data.response[391]);
                assert.equal('object', typeof data.response[392]);
                assert.equal('object', typeof data.response[393]);
                assert.equal('object', typeof data.response[394]);
                assert.equal('object', typeof data.response[395]);
                assert.equal('object', typeof data.response[396]);
                assert.equal('object', typeof data.response[397]);
                assert.equal('object', typeof data.response[398]);
                assert.equal('object', typeof data.response[399]);
                assert.equal('object', typeof data.response[400]);
                assert.equal('object', typeof data.response[401]);
                assert.equal('object', typeof data.response[402]);
                assert.equal('object', typeof data.response[403]);
                assert.equal('object', typeof data.response[404]);
                assert.equal('object', typeof data.response[405]);
                assert.equal('object', typeof data.response[406]);
                assert.equal('object', typeof data.response[407]);
                assert.equal('object', typeof data.response[408]);
                assert.equal('object', typeof data.response[409]);
                assert.equal('object', typeof data.response[410]);
                assert.equal('object', typeof data.response[411]);
                assert.equal('object', typeof data.response[412]);
                assert.equal('object', typeof data.response[413]);
                assert.equal('object', typeof data.response[414]);
                assert.equal('object', typeof data.response[415]);
                assert.equal('object', typeof data.response[416]);
                assert.equal('object', typeof data.response[417]);
                assert.equal('object', typeof data.response[418]);
                assert.equal('object', typeof data.response[419]);
                assert.equal('object', typeof data.response[420]);
                assert.equal('object', typeof data.response[421]);
                assert.equal('object', typeof data.response[422]);
                assert.equal('object', typeof data.response[423]);
                assert.equal('object', typeof data.response[424]);
                assert.equal('object', typeof data.response[425]);
                assert.equal('object', typeof data.response[426]);
                assert.equal('object', typeof data.response[427]);
                assert.equal('object', typeof data.response[428]);
                assert.equal('object', typeof data.response[429]);
                assert.equal('object', typeof data.response[430]);
                assert.equal('object', typeof data.response[431]);
                assert.equal('object', typeof data.response[432]);
                assert.equal('object', typeof data.response[433]);
                assert.equal('object', typeof data.response[434]);
                assert.equal('object', typeof data.response[435]);
                assert.equal('object', typeof data.response[436]);
                assert.equal('object', typeof data.response[437]);
                assert.equal('object', typeof data.response[438]);
                assert.equal('object', typeof data.response[439]);
                assert.equal('object', typeof data.response[440]);
                assert.equal('object', typeof data.response[441]);
                assert.equal('object', typeof data.response[442]);
                assert.equal('object', typeof data.response[443]);
                assert.equal('object', typeof data.response[444]);
                assert.equal('object', typeof data.response[445]);
                assert.equal('object', typeof data.response[446]);
                assert.equal('object', typeof data.response[447]);
                assert.equal('object', typeof data.response[448]);
                assert.equal('object', typeof data.response[449]);
                assert.equal('object', typeof data.response[450]);
                assert.equal('object', typeof data.response[451]);
                assert.equal('object', typeof data.response[452]);
                assert.equal('object', typeof data.response[453]);
                assert.equal('object', typeof data.response[454]);
                assert.equal('object', typeof data.response[455]);
                assert.equal('object', typeof data.response[456]);
                assert.equal('object', typeof data.response[457]);
                assert.equal('object', typeof data.response[458]);
                assert.equal('object', typeof data.response[459]);
                assert.equal('object', typeof data.response[460]);
                assert.equal('object', typeof data.response[461]);
                assert.equal('object', typeof data.response[462]);
                assert.equal('object', typeof data.response[463]);
                assert.equal('object', typeof data.response[464]);
                assert.equal('object', typeof data.response[465]);
                assert.equal('object', typeof data.response[466]);
                assert.equal('object', typeof data.response[467]);
                assert.equal('object', typeof data.response[468]);
                assert.equal('object', typeof data.response[469]);
                assert.equal('object', typeof data.response[470]);
                assert.equal('object', typeof data.response[471]);
                assert.equal('object', typeof data.response[472]);
                assert.equal('object', typeof data.response[473]);
                assert.equal('object', typeof data.response[474]);
                assert.equal('object', typeof data.response[475]);
                assert.equal('object', typeof data.response[476]);
                assert.equal('object', typeof data.response[477]);
                assert.equal('object', typeof data.response[478]);
                assert.equal('object', typeof data.response[479]);
                assert.equal('object', typeof data.response[480]);
                assert.equal('object', typeof data.response[481]);
                assert.equal('object', typeof data.response[482]);
                assert.equal('object', typeof data.response[483]);
                assert.equal('object', typeof data.response[484]);
                assert.equal('object', typeof data.response[485]);
                assert.equal('object', typeof data.response[486]);
                assert.equal('object', typeof data.response[487]);
                assert.equal('object', typeof data.response[488]);
                assert.equal('object', typeof data.response[489]);
                assert.equal('object', typeof data.response[490]);
                assert.equal('object', typeof data.response[491]);
                assert.equal('object', typeof data.response[492]);
                assert.equal('object', typeof data.response[493]);
                assert.equal('object', typeof data.response[494]);
                assert.equal('object', typeof data.response[495]);
                assert.equal('object', typeof data.response[496]);
                assert.equal('object', typeof data.response[497]);
                assert.equal('object', typeof data.response[498]);
                assert.equal('object', typeof data.response[499]);
                assert.equal('object', typeof data.response[500]);
                assert.equal('object', typeof data.response[501]);
                assert.equal('object', typeof data.response[502]);
                assert.equal('object', typeof data.response[503]);
                assert.equal('object', typeof data.response[504]);
                assert.equal('object', typeof data.response[505]);
                assert.equal('object', typeof data.response[506]);
                assert.equal('object', typeof data.response[507]);
                assert.equal('object', typeof data.response[508]);
                assert.equal('object', typeof data.response[509]);
                assert.equal('object', typeof data.response[510]);
                assert.equal('object', typeof data.response[511]);
                assert.equal('object', typeof data.response[512]);
                assert.equal('object', typeof data.response[513]);
                assert.equal('object', typeof data.response[514]);
                assert.equal('object', typeof data.response[515]);
                assert.equal('object', typeof data.response[516]);
                assert.equal('object', typeof data.response[517]);
                assert.equal('object', typeof data.response[518]);
                assert.equal('object', typeof data.response[519]);
                assert.equal('object', typeof data.response[520]);
                assert.equal('object', typeof data.response[521]);
                assert.equal('object', typeof data.response[522]);
                assert.equal('object', typeof data.response[523]);
                assert.equal('object', typeof data.response[524]);
                assert.equal('object', typeof data.response[525]);
                assert.equal('object', typeof data.response[526]);
                assert.equal('object', typeof data.response[527]);
                assert.equal('object', typeof data.response[528]);
                assert.equal('object', typeof data.response[529]);
                assert.equal('object', typeof data.response[530]);
                assert.equal('object', typeof data.response[531]);
                assert.equal('object', typeof data.response[532]);
                assert.equal('object', typeof data.response[533]);
                assert.equal('object', typeof data.response[534]);
                assert.equal('object', typeof data.response[535]);
                assert.equal('object', typeof data.response[536]);
                assert.equal('object', typeof data.response[537]);
                assert.equal('object', typeof data.response[538]);
                assert.equal('object', typeof data.response[539]);
                assert.equal('object', typeof data.response[540]);
                assert.equal('object', typeof data.response[541]);
                assert.equal('object', typeof data.response[542]);
                assert.equal('object', typeof data.response[543]);
                assert.equal('object', typeof data.response[544]);
                assert.equal('object', typeof data.response[545]);
                assert.equal('object', typeof data.response[546]);
                assert.equal('object', typeof data.response[547]);
                assert.equal('object', typeof data.response[548]);
                assert.equal('object', typeof data.response[549]);
                assert.equal('object', typeof data.response[550]);
                assert.equal('object', typeof data.response[551]);
                assert.equal('object', typeof data.response[552]);
                assert.equal('object', typeof data.response[553]);
                assert.equal('object', typeof data.response[554]);
                assert.equal('object', typeof data.response[555]);
                assert.equal('object', typeof data.response[556]);
                assert.equal('object', typeof data.response[557]);
                assert.equal('object', typeof data.response[558]);
                assert.equal('object', typeof data.response[559]);
                assert.equal('object', typeof data.response[560]);
                assert.equal('object', typeof data.response[561]);
                assert.equal('object', typeof data.response[562]);
                assert.equal('object', typeof data.response[563]);
                assert.equal('object', typeof data.response[564]);
                assert.equal('object', typeof data.response[565]);
                assert.equal('object', typeof data.response[566]);
                assert.equal('object', typeof data.response[567]);
                assert.equal('object', typeof data.response[568]);
                assert.equal('object', typeof data.response[569]);
                assert.equal('object', typeof data.response[570]);
                assert.equal('object', typeof data.response[571]);
                assert.equal('object', typeof data.response[572]);
                assert.equal('object', typeof data.response[573]);
                assert.equal('object', typeof data.response[574]);
                assert.equal('object', typeof data.response[575]);
                assert.equal('object', typeof data.response[576]);
                assert.equal('object', typeof data.response[577]);
                assert.equal('object', typeof data.response[578]);
                assert.equal('object', typeof data.response[579]);
                assert.equal('object', typeof data.response[580]);
                assert.equal('object', typeof data.response[581]);
                assert.equal('object', typeof data.response[582]);
                assert.equal('object', typeof data.response[583]);
                assert.equal('object', typeof data.response[584]);
                assert.equal('object', typeof data.response[585]);
                assert.equal('object', typeof data.response[586]);
                assert.equal('object', typeof data.response[587]);
                assert.equal('object', typeof data.response[588]);
                assert.equal('object', typeof data.response[589]);
                assert.equal('object', typeof data.response[590]);
                assert.equal('object', typeof data.response[591]);
                assert.equal('object', typeof data.response[592]);
                assert.equal('object', typeof data.response[593]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'schedulesSchedulesZoneinfoList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulesSchedulesSchedulesUpdateBodyParam = {
      name: 'string',
      rrule: 'string',
      unified_job_template: 3
    };
    describe('#schedulesSchedulesUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.schedulesSchedulesUpdate(schedulesId, null, schedulesSchedulesSchedulesUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'schedulesSchedulesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const schedulesSchedulesSchedulesPartialUpdateBodyParam = {};
    describe('#schedulesSchedulesPartialUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.schedulesSchedulesPartialUpdate(schedulesId, null, schedulesSchedulesSchedulesPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'schedulesSchedulesPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.schedulesSchedulesRead(schedulesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'schedulesSchedulesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesCredentialsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.schedulesSchedulesCredentialsList(schedulesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'schedulesSchedulesCredentialsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesJobsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.schedulesSchedulesJobsList(schedulesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'schedulesSchedulesJobsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const settingsSettingsSettingsLoggingTestCreateBodyParam = {};
    describe('#settingsSettingsLoggingTestCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.settingsSettingsLoggingTestCreate(settingsSettingsSettingsLoggingTestCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'settingsSettingsLoggingTestCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#settingsSettingsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.settingsSettingsList(null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'settingsSettingsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const settingsCategorySlug = 'fakedata';
    const settingsSettingsSettingsUpdateBodyParam = {};
    describe('#settingsSettingsUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.settingsSettingsUpdate(settingsCategorySlug, settingsSettingsSettingsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'settingsSettingsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const settingsSettingsSettingsPartialUpdateBodyParam = {};
    describe('#settingsSettingsPartialUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.settingsSettingsPartialUpdate(settingsCategorySlug, settingsSettingsSettingsPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'settingsSettingsPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#settingsSettingsRead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.settingsSettingsRead(settingsCategorySlug, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('', data.response.CUSTOM_LOGIN_INFO);
                assert.equal('', data.response.CUSTOM_LOGO);
                assert.equal(4000, data.response.MAX_UI_JOB_EVENTS);
                assert.equal('off', data.response.PENDO_TRACKING_STATE);
                assert.equal(true, data.response.UI_LIVE_UPDATES_ENABLED);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'settingsSettingsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemJobTemplatesId = 'fakedata';
    describe('#systemJobTemplatesSystemJobTemplatesLaunchCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesLaunchCreate(systemJobTemplatesId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobTemplates', 'systemJobTemplatesSystemJobTemplatesLaunchCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemJobTemplatesSystemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorCreateBodyParam = {
      name: 'string',
      notification_type: 'string',
      organization: 7
    };
    describe('#systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorCreate(systemJobTemplatesId, systemJobTemplatesSystemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobTemplates', 'systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemJobTemplatesSystemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedCreateBodyParam = {};
    describe('#systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedCreate(systemJobTemplatesId, systemJobTemplatesSystemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobTemplates', 'systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemJobTemplatesSystemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessCreateBodyParam = {
      name: 'string',
      notification_type: 'string',
      organization: 4
    };
    describe('#systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessCreate(systemJobTemplatesId, systemJobTemplatesSystemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobTemplates', 'systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemJobTemplatesSystemJobTemplatesSystemJobTemplatesSchedulesCreateBodyParam = {
      name: 'string',
      rrule: 'string',
      unified_job_template: 1
    };
    describe('#systemJobTemplatesSystemJobTemplatesSchedulesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesSchedulesCreate(systemJobTemplatesId, systemJobTemplatesSystemJobTemplatesSystemJobTemplatesSchedulesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobTemplates', 'systemJobTemplatesSystemJobTemplatesSchedulesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobTemplates', 'systemJobTemplatesSystemJobTemplatesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesRead(systemJobTemplatesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobTemplates', 'systemJobTemplatesSystemJobTemplatesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesJobsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesJobsList(systemJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobTemplates', 'systemJobTemplatesSystemJobTemplatesJobsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesLaunchList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesLaunchList(systemJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobTemplates', 'systemJobTemplatesSystemJobTemplatesLaunchList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorList(systemJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobTemplates', 'systemJobTemplatesSystemJobTemplatesNotificationTemplatesErrorList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedList(systemJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobTemplates', 'systemJobTemplatesSystemJobTemplatesNotificationTemplatesStartedList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessList(systemJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobTemplates', 'systemJobTemplatesSystemJobTemplatesNotificationTemplatesSuccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobTemplatesSystemJobTemplatesSchedulesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobTemplatesSystemJobTemplatesSchedulesList(systemJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobTemplates', 'systemJobTemplatesSystemJobTemplatesSchedulesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemJobsSystemJobsSystemJobsCreateBodyParam = {
      name: 'string'
    };
    describe('#systemJobsSystemJobsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobsSystemJobsCreate(systemJobsSystemJobsSystemJobsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobs', 'systemJobsSystemJobsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemJobsId = 'fakedata';
    describe('#systemJobsSystemJobsCancelCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobsSystemJobsCancelCreate(systemJobsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobs', 'systemJobsSystemJobsCancelCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobsSystemJobsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobsSystemJobsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobs', 'systemJobsSystemJobsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobsSystemJobsRead - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.systemJobsSystemJobsRead(systemJobsId, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal(0, data.response.elapsed);
                assert.equal(false, data.response.event_processing_finished);
                assert.equal('', data.response.execution_node);
                assert.equal('', data.response.extra_vars);
                assert.equal(false, data.response.failed);
                assert.equal(1, data.response.id);
                assert.equal('', data.response.job_args);
                assert.equal('', data.response.job_cwd);
                assert.equal('object', typeof data.response.job_env);
                assert.equal('', data.response.job_explanation);
                assert.equal('', data.response.job_type);
                assert.equal('manual', data.response.launch_type);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('Standard Output too large to display (1048577 bytes), only download supported for sizes over 1048576 bytes.', data.response.result_stdout);
                assert.equal('', data.response.result_traceback);
                assert.equal('new', data.response.status);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('system_job', data.response.type);
                assert.equal('/api/v2/system_jobs/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobs', 'systemJobsSystemJobsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobsSystemJobsCancelRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobsSystemJobsCancelRead(systemJobsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobs', 'systemJobsSystemJobsCancelRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobsSystemJobsEventsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobsSystemJobsEventsList(systemJobsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobs', 'systemJobsSystemJobsEventsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobsSystemJobsNotificationsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobsSystemJobsNotificationsList(systemJobsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobs', 'systemJobsSystemJobsNotificationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsTeamsTeamsCreateBodyParam = {
      name: 'string',
      organization: 3
    };
    describe('#teamsTeamsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.teamsTeamsCreate(teamsTeamsTeamsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'teamsTeamsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let teamsId = 'fakedata';
    const teamsTeamsTeamsCredentialsCreateBodyParam = {};
    describe('#teamsTeamsCredentialsCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.teamsTeamsCredentialsCreate(teamsId, teamsTeamsTeamsCredentialsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(false, data.response.cloud);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal(1, data.response.credential_type);
                assert.equal('', data.response.description);
                assert.equal(1, data.response.id);
                assert.equal('object', typeof data.response.inputs);
                assert.equal('ssh', data.response.kind);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('Some name', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('credential', data.response.type);
                assert.equal('/api/v2/credentials/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              teamsId = data.response.id;
              saveMockData('Teams', 'teamsTeamsCredentialsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsTeamsTeamsRolesCreateBodyParam = {};
    describe('#teamsTeamsRolesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.teamsTeamsRolesCreate(teamsId, teamsTeamsTeamsRolesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'teamsTeamsRolesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsTeamsTeamsUsersCreateBodyParam = {
      username: 'string'
    };
    describe('#teamsTeamsUsersCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.teamsTeamsUsersCreate(teamsId, teamsTeamsTeamsUsersCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'teamsTeamsUsersCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.teamsTeamsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'teamsTeamsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsTeamsTeamsUpdateBodyParam = {
      name: 'string',
      organization: 4
    };
    describe('#teamsTeamsUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.teamsTeamsUpdate(teamsId, null, teamsTeamsTeamsUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'teamsTeamsUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const teamsTeamsTeamsPartialUpdateBodyParam = {
      description: 'string',
      name: 'string',
      organization: 5
    };
    describe('#teamsTeamsPartialUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.teamsTeamsPartialUpdate(teamsId, null, teamsTeamsTeamsPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'teamsTeamsPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.teamsTeamsRead(teamsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'teamsTeamsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsAccessListList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.teamsTeamsAccessListList(teamsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'teamsTeamsAccessListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.teamsTeamsActivityStreamList(teamsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'teamsTeamsActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsCredentialsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.teamsTeamsCredentialsList(teamsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'teamsTeamsCredentialsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsObjectRolesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.teamsTeamsObjectRolesList(teamsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'teamsTeamsObjectRolesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsProjectsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.teamsTeamsProjectsList(teamsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'teamsTeamsProjectsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsRolesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.teamsTeamsRolesList(teamsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'teamsTeamsRolesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsUsersList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.teamsTeamsUsersList(teamsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'teamsTeamsUsersList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unifiedJobTemplatesUnifiedJobTemplatesList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.unifiedJobTemplatesUnifiedJobTemplatesList(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(0, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UnifiedJobTemplates', 'unifiedJobTemplatesUnifiedJobTemplatesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#unifiedJobsUnifiedJobsList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.unifiedJobsUnifiedJobsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UnifiedJobs', 'unifiedJobsUnifiedJobsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobNodesWorkflowJobNodesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobNodesWorkflowJobNodesList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobNodes', 'workflowJobNodesWorkflowJobNodesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobNodesId = 'fakedata';
    describe('#workflowJobNodesWorkflowJobNodesRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobNodesWorkflowJobNodesRead(workflowJobNodesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobNodes', 'workflowJobNodesWorkflowJobNodesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobNodesWorkflowJobNodesAlwaysNodesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobNodesWorkflowJobNodesAlwaysNodesList(workflowJobNodesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobNodes', 'workflowJobNodesWorkflowJobNodesAlwaysNodesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobNodesWorkflowJobNodesCredentialsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobNodesWorkflowJobNodesCredentialsList(workflowJobNodesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobNodes', 'workflowJobNodesWorkflowJobNodesCredentialsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobNodesWorkflowJobNodesFailureNodesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobNodesWorkflowJobNodesFailureNodesList(workflowJobNodesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobNodes', 'workflowJobNodesWorkflowJobNodesFailureNodesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobNodesWorkflowJobNodesSuccessNodesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobNodesWorkflowJobNodesSuccessNodesList(workflowJobNodesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobNodes', 'workflowJobNodesWorkflowJobNodesSuccessNodesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplateNodesWorkflowJobTemplateNodesWorkflowJobTemplateNodesCreateBodyParam = {
      workflow_job_template: 'string'
    };
    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesCreate(workflowJobTemplateNodesWorkflowJobTemplateNodesWorkflowJobTemplateNodesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplateNodes', 'workflowJobTemplateNodesWorkflowJobTemplateNodesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplateNodesId = 'fakedata';
    const workflowJobTemplateNodesWorkflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesCreateBodyParam = {};
    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesCreate(workflowJobTemplateNodesId, workflowJobTemplateNodesWorkflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplateNodes', 'workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplateNodesWorkflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsCreateBodyParam = {
      credential_type: 3,
      name: 'string'
    };
    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsCreate(workflowJobTemplateNodesId, workflowJobTemplateNodesWorkflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplateNodes', 'workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplateNodesWorkflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesCreateBodyParam = {};
    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesCreate(workflowJobTemplateNodesId, workflowJobTemplateNodesWorkflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplateNodes', 'workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplateNodesWorkflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesCreateBodyParam = {};
    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesCreate(workflowJobTemplateNodesId, workflowJobTemplateNodesWorkflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplateNodes', 'workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplateNodes', 'workflowJobTemplateNodesWorkflowJobTemplateNodesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplateNodesWorkflowJobTemplateNodesWorkflowJobTemplateNodesUpdateBodyParam = {
      workflow_job_template: 'string'
    };
    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesUpdate(workflowJobTemplateNodesId, null, workflowJobTemplateNodesWorkflowJobTemplateNodesWorkflowJobTemplateNodesUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplateNodes', 'workflowJobTemplateNodesWorkflowJobTemplateNodesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplateNodesWorkflowJobTemplateNodesWorkflowJobTemplateNodesPartialUpdateBodyParam = {};
    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesPartialUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesPartialUpdate(workflowJobTemplateNodesId, null, workflowJobTemplateNodesWorkflowJobTemplateNodesWorkflowJobTemplateNodesPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('success', data.response);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplateNodes', 'workflowJobTemplateNodesWorkflowJobTemplateNodesPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesRead(workflowJobTemplateNodesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplateNodes', 'workflowJobTemplateNodesWorkflowJobTemplateNodesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesList(workflowJobTemplateNodesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplateNodes', 'workflowJobTemplateNodesWorkflowJobTemplateNodesAlwaysNodesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsList(workflowJobTemplateNodesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplateNodes', 'workflowJobTemplateNodesWorkflowJobTemplateNodesCredentialsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesList(workflowJobTemplateNodesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplateNodes', 'workflowJobTemplateNodesWorkflowJobTemplateNodesFailureNodesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesList(workflowJobTemplateNodesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplateNodes', 'workflowJobTemplateNodesWorkflowJobTemplateNodesSuccessNodesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesCreateBodyParam = {
      name: 'string'
    };
    describe('#workflowJobTemplatesWorkflowJobTemplatesCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesCreate(workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplatesId = 'fakedata';
    const workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesCopyCreateBodyParam = {
      name: 'string'
    };
    describe('#workflowJobTemplatesWorkflowJobTemplatesCopyCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesCopyCreate(workflowJobTemplatesId, workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesCopyCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesCopyCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesLabelsCreateBodyParam = {
      name: 'string',
      organization: 8
    };
    describe('#workflowJobTemplatesWorkflowJobTemplatesLabelsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesLabelsCreate(workflowJobTemplatesId, workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesLabelsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesLabelsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesLaunchCreateBodyParam = {
      extra_vars: 'string',
      inventory: 5
    };
    describe('#workflowJobTemplatesWorkflowJobTemplatesLaunchCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesLaunchCreate(workflowJobTemplatesId, workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesLaunchCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesLaunchCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorCreateBodyParam = {
      name: 'string',
      notification_type: 'string',
      organization: 3
    };
    describe('#workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorCreate(workflowJobTemplatesId, workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedCreateBodyParam = {};
    describe('#workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedCreate(workflowJobTemplatesId, workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessCreateBodyParam = {
      name: 'string',
      notification_type: 'string',
      organization: 2
    };
    describe('#workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessCreate(workflowJobTemplatesId, workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesSchedulesCreateBodyParam = {};
    describe('#workflowJobTemplatesWorkflowJobTemplatesSchedulesCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesSchedulesCreate(workflowJobTemplatesId, workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesSchedulesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('', data.response.description);
                assert.equal('2015-11-17T05:00:00Z', data.response.dtend);
                assert.equal('2015-11-17T05:00:00Z', data.response.dtstart);
                assert.equal(true, data.response.enabled);
                assert.equal('object', typeof data.response.extra_data);
                assert.equal(1, data.response.id);
                assert.equal(1, data.response.inventory);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('test sch', data.response.name);
                assert.equal('object', typeof data.response.related);
                assert.equal('DTSTART:20151117T050000Z RRULE:FREQ=DAILY;INTERVAL=1;COUNT=1', data.response.rrule);
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('UTC', data.response.timezone);
                assert.equal('schedule', data.response.type);
                assert.equal(1, data.response.unified_job_template);
                assert.equal('', data.response.until);
                assert.equal('/api/v2/schedules/1/', data.response.url);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesSchedulesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesSurveySpecCreateBodyParam = {};
    describe('#workflowJobTemplatesWorkflowJobTemplatesSurveySpecCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesSurveySpecCreate(workflowJobTemplatesId, workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesSurveySpecCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesSurveySpecCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesWorkflowNodesCreateBodyParam = {};
    describe('#workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesCreate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesCreate(workflowJobTemplatesId, workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesWorkflowNodesCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(true, Array.isArray(data.response.always_nodes));
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.created);
                assert.equal('object', typeof data.response.extra_data);
                assert.equal(true, Array.isArray(data.response.failure_nodes));
                assert.equal(1, data.response.id);
                assert.equal('webservers', data.response.limit);
                assert.equal('2018-02-01T08:00:00.000000Z', data.response.modified);
                assert.equal('object', typeof data.response.related);
                assert.equal(true, Array.isArray(data.response.success_nodes));
                assert.equal('object', typeof data.response.summary_fields);
                assert.equal('workflow_job_template_node', data.response.type);
                assert.equal(3, data.response.unified_job_template);
                assert.equal('/api/v2/workflow_job_template_nodes/1/', data.response.url);
                assert.equal(2, data.response.workflow_job_template);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesUpdateBodyParam = {
      name: 'string'
    };
    describe('#workflowJobTemplatesWorkflowJobTemplatesUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesUpdate(workflowJobTemplatesId, null, workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesPartialUpdateBodyParam = {
      allow_simultaneous: 'string',
      ask_inventory_on_launch: 'string',
      ask_variables_on_launch: 'string',
      description: 'string',
      extra_vars: 'string',
      inventory: 2,
      name: 'string',
      organization: 7,
      survey_enabled: 'string'
    };
    describe('#workflowJobTemplatesWorkflowJobTemplatesPartialUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesPartialUpdate(workflowJobTemplatesId, null, workflowJobTemplatesWorkflowJobTemplatesWorkflowJobTemplatesPartialUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesPartialUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesRead(workflowJobTemplatesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesAccessListList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesAccessListList(workflowJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesAccessListList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesActivityStreamList(workflowJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesCopyList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesCopyList(workflowJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesCopyList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesLabelsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesLabelsList(workflowJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesLabelsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesLaunchRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesLaunchRead(workflowJobTemplatesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesLaunchRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorList(workflowJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesErrorList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedList - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedList(workflowJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                runCommonAsserts(data, error);
                assert.equal(1, data.response.count);
                assert.equal(true, Array.isArray(data.response.results));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesStartedList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessList(workflowJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesNotificationTemplatesSuccessList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesObjectRolesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesObjectRolesList(workflowJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesObjectRolesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesSchedulesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesSchedulesList(workflowJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesSchedulesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesSurveySpecList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesSurveySpecList(workflowJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesSurveySpecList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesWorkflowJobsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesWorkflowJobsList(workflowJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesWorkflowJobsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesList(workflowJobTemplatesId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesWorkflowNodesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobsWorkflowJobsWorkflowJobsCreateBodyParam = {
      name: 'string'
    };
    describe('#workflowJobsWorkflowJobsCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobsWorkflowJobsCreate(workflowJobsWorkflowJobsWorkflowJobsCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobs', 'workflowJobsWorkflowJobsCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const workflowJobsId = 'fakedata';
    describe('#workflowJobsWorkflowJobsCancelCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobsWorkflowJobsCancelCreate(workflowJobsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobs', 'workflowJobsWorkflowJobsCancelCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsRelaunchCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobsWorkflowJobsRelaunchCreate(workflowJobsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobs', 'workflowJobsWorkflowJobsRelaunchCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobsWorkflowJobsList(null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobs', 'workflowJobsWorkflowJobsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobsWorkflowJobsRead(workflowJobsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobs', 'workflowJobsWorkflowJobsRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsActivityStreamList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobsWorkflowJobsActivityStreamList(workflowJobsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobs', 'workflowJobsWorkflowJobsActivityStreamList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsCancelRead - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobsWorkflowJobsCancelRead(workflowJobsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobs', 'workflowJobsWorkflowJobsCancelRead', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsLabelsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobsWorkflowJobsLabelsList(workflowJobsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobs', 'workflowJobsWorkflowJobsLabelsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsNotificationsList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobsWorkflowJobsNotificationsList(workflowJobsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobs', 'workflowJobsWorkflowJobsNotificationsList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsRelaunchList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobsWorkflowJobsRelaunchList(workflowJobsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobs', 'workflowJobsWorkflowJobsRelaunchList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsWorkflowNodesList - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobsWorkflowJobsWorkflowNodesList(workflowJobsId, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobs', 'workflowJobsWorkflowJobsWorkflowNodesList', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationApplicationsDelete0 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authenticationApplicationsDelete0(authenticationId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticationApplicationsDelete0', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authenticationTokensDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authenticationTokensDelete(authenticationId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authenticationTokensDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adHocCommandsAdHocCommandsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.adHocCommandsAdHocCommandsDelete(adHocCommandsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AdHocCommands', 'adHocCommandsAdHocCommandsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemConfigurationConfigDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemConfigurationConfigDelete((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemConfiguration', 'systemConfigurationConfigDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialInputSourcesCredentialInputSourcesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialInputSourcesCredentialInputSourcesDelete(credentialInputSourcesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CredentialInputSources', 'credentialInputSourcesCredentialInputSourcesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialTypesCredentialTypesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialTypesCredentialTypesDelete(credentialTypesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CredentialTypes', 'credentialTypesCredentialTypesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#credentialsCredentialsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.credentialsCredentialsDelete(credentialsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Credentials', 'credentialsCredentialsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupsGroupsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupsGroupsDelete(groupsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Groups', 'groupsGroupsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostsHostsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hostsHostsDelete(hostsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hosts', 'hostsHostsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#instanceGroupsInstanceGroupsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.instanceGroupsInstanceGroupsDelete(instanceGroupsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InstanceGroups', 'instanceGroupsInstanceGroupsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoriesInventoriesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoriesInventoriesDelete(inventoriesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Inventories', 'inventoriesInventoriesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customInventoryScriptsInventoryScriptsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.customInventoryScriptsInventoryScriptsDelete(customInventoryScriptsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CustomInventoryScripts', 'customInventoryScriptsInventoryScriptsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesDelete(inventorySourcesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesGroupsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesGroupsDelete(inventorySourcesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesGroupsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventorySourcesInventorySourcesHostsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventorySourcesInventorySourcesHostsDelete(inventorySourcesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventorySources', 'inventorySourcesInventorySourcesHostsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#inventoryUpdatesInventoryUpdatesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.inventoryUpdatesInventoryUpdatesDelete(inventoryUpdatesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InventoryUpdates', 'inventoryUpdatesInventoryUpdatesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesDelete(jobTemplatesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobTemplatesJobTemplatesSurveySpecDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobTemplatesJobTemplatesSurveySpecDelete(jobTemplatesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('JobTemplates', 'jobTemplatesJobTemplatesSurveySpecDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jobsJobsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.jobsJobsDelete(jobsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Jobs', 'jobsJobsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#usersUsersDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.usersUsersDelete(usersId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'usersUsersDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#notificationTemplatesNotificationTemplatesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.notificationTemplatesNotificationTemplatesDelete(notificationTemplatesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NotificationTemplates', 'notificationTemplatesNotificationTemplatesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#organizationsOrganizationsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.organizationsOrganizationsDelete(organizationsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Organizations', 'organizationsOrganizationsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectUpdatesProjectUpdatesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectUpdatesProjectUpdatesDelete(projectUpdatesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ProjectUpdates', 'projectUpdatesProjectUpdatesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#projectsProjectsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.projectsProjectsDelete(projectsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Projects', 'projectsProjectsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#schedulesSchedulesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.schedulesSchedulesDelete(schedulesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Schedules', 'schedulesSchedulesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#settingsSettingsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.settingsSettingsDelete(settingsCategorySlug, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Settings', 'settingsSettingsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#systemJobsSystemJobsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.systemJobsSystemJobsDelete(systemJobsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemJobs', 'systemJobsSystemJobsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#teamsTeamsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.teamsTeamsDelete(teamsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Teams', 'teamsTeamsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplateNodesWorkflowJobTemplateNodesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplateNodesWorkflowJobTemplateNodesDelete(workflowJobTemplateNodesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplateNodes', 'workflowJobTemplateNodesWorkflowJobTemplateNodesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesDelete(workflowJobTemplatesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobTemplatesWorkflowJobTemplatesSurveySpecDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobTemplatesWorkflowJobTemplatesSurveySpecDelete(workflowJobTemplatesId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobTemplates', 'workflowJobTemplatesWorkflowJobTemplatesSurveySpecDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#workflowJobsWorkflowJobsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.workflowJobsWorkflowJobsDelete(workflowJobsId, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-ansible_tower-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('WorkflowJobs', 'workflowJobsWorkflowJobsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
