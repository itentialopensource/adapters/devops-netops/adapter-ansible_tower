# Ansible Tower

Vendor: Red Hat
Homepage: https://www.redhat.com/en

Product: Ansible Tower
Product Page: https://www.redhat.com/en/technologies/management/ansible/automation-controller

## Introduction
We classify Ansible Tower into the CI/CD domain as Ansible Tower provides centralized management, orchestration and monitoring of Ansible automation workflows.

## Why Integrate
The Ansible Tower adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Red Hat Ansible Tower to enable automation and integration with other tools and systems

With this adapter you have the ability to perform operations with Ansible Tower such as:

- Get Job Template
- Launch Job Template
- Get Job
- Cancel Job
- Create Job Schedule
- Cancel Job Schedule

## Additional Product Documentation
The [API documents for Ansible Tower](https://docs.ansible.com/ansible-tower/latest/html/towerapi/index.html)
